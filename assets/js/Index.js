import "/assets/js/components/HeaderSearch.js";
import "/assets/js/components/HeaderFormField.js";
import "/assets/js/common/Modal.js";
import "/assets/js/queries/Query.js";
import "/assets/js/queries/FeedbackQuery.js";
import "/assets/js/queries/ProductsQueries.js";
import "/assets/js/common/Cookier.js"
import "/assets/js/common/Cart.js"
import "/assets/js/common/PhoneMask.js"
import "/assets/js/utils/formateDate.js"
import "/assets/js/utils/getNumberPhone.js"
import "/assets/js/common/FormValidator.js"
import "/assets/js/queries/OrderQuery.js"
import "/assets/js/queries/AuthQueries.js"
import "/assets/js/queries/ConfirmPhoneOrder.js"
import "/assets/js/utils/localStorageExplorer.js"
import "/assets/js/components/CartItem.js";
import "/assets/js/components/CategoryItem.js";
import "/assets/js/components/CatalogProductItem.js";
import "/assets/js/catalog/PointMenu.js"
import "/assets/js/catalog/SelectParts.js"
import "/assets/js/common/Tabs.js";
import "/assets/js/queries/CartQueries.js"
import "/assets/js/utils/arrayEquals.js";
import "/assets/js/components/OrderForm.js"
import "/assets/js/common/Cart.js";
import "/assets/js/common/SMSlength.js";
import "/assets/js/payment/Payment.js"
import "/assets/js/queries/PaymentQueries.js"

import Modals from '/assets/js/common/InitModals.js'
import Search from "/assets/js/common/Search.js";
import getPageId from "/assets/js/utils/getPageId.js";
import MainSliders from "/assets/js/main/Sliders.js";
import Feedback from "./common/Feedback.js";
import InputsState from "/assets/js/common/InputsState.js"
import SelectableFields from "/assets/js/common/Selectable.js";
import DatePicker from "/assets/js/common/DatePicker.js";
import OrderForm from "/assets/js/forms/order/OrderForm.js";
import RegisterForm from "/assets/js/forms/register/RegisterForm.js";
import LoginForm from "/assets/js/forms/login/LoginForm.js";
import Product from "/assets/js/product/Product.js";
import Catalog from "/assets/js/catalog/CatalogInit.js";
import Profile from "/assets/js/profile/ProfileInit.js";
import {phoneFieldMask} from "/assets/js/common/PhoneMask.js";
import VideoBackground from "/assets/js/main/Video.js";
import Payment from '/assets/js/payment/Payment.js'
import {initCartData} from  "/assets/js/common/Cart.js";


document.addEventListener('DOMContentLoaded', async function () {
    const pageId = getPageId()

    const importsBox = {
        1: [
            MainSliders.promoSlidersInit,
            Search.initMain,
            MainSliders.promoMobileSliderInit,
            MainSliders.init,
            MainSliders.slidesTextCorrect,

        ],
        2: [
            Catalog.init
        ],
        3: [
            Product.init
        ],
        4: [],
        5: [],
        6: [
            OrderForm.init,
        ],
        7: [
        ],
        8: [
            LoginForm.init
        ],
        9: [
            SelectableFields.init,
            RegisterForm.init,
            DatePicker.registerInit
        ],
        10: [
            //DatePicker.profileInit,
            //SelectableFields.init,
            Profile.init,
        ],
        13: [
            Payment.init
        ]
    }
    const initImportBox = () => {
        importsBox[pageId].forEach(async moduleInitialize => await moduleInitialize())
    }
    //await initCartData(initImportBox)
    initImportBox()

// Common modules
// Init modules which should work in all pages

    Search.init() // Searching in header
    Modals.init() // All modals initialization
    Feedback.init() // Init feedback form
    InputsState.init() // Inputs bordering and clearing
    phoneFieldMask() // Mask phone where it's necessary
})
