import {getProductsByCategory} from "/assets/js/queries/ProductsQueries.js";
import CategoryItem from "/assets/js/components/CategoryItem.js";
import {getCookie, setCookie} from "/assets/js/common/Cookier.js";
import CatalogProductItem from "/assets/js/components/CatalogProductItem.js";
import {addToCart, getCartData, isProductInCart, renderAdded} from "/assets/js/common/Cart.js";
import "/assets/js/catalog/PointMenu.js"
import "/assets/js/catalog/SelectParts.js"


const Catalog = {
    init() {

        let catalogPart = 1
        let isCanLoaded = true

        const catalogSearchBlocks = document.querySelectorAll(".ctg__search_input")
        const categoriesDesktopBlocks = document.querySelectorAll(".ctg__group_list");
        const catalogDesktopList = document.getElementById("catalog-desktop-list")
        const loadBtn = document.getElementById("catalogLoadBtn")

        const url = window.location.href;
        const urlParams = new URLSearchParams(url);

        const catParam = urlParams.get('cat');
        const catArray = catParam ? catParam.split(',').map(item => !isNaN(Number(item)) ? Number(item) : null) : [];
        const catNormalizedArr = catArray.filter(item => item !== null)

        const catalogFilter = JSON.parse(getCookie("catalogFilter")) || {
            search: "",
            categories: catNormalizedArr,
        };

        const setCatalogFilter = (search = "", categories = [], cookieSaving = true) => {
            catalogFilter.search = search
            catalogFilter.categories = categories

            if (cookieSaving) {
                setCookie("catalogFilter", JSON.stringify(catalogFilter), 7);
            }

            const result = JSON.parse(getCookie("catalogFilter"))
            urlParams.set("cat", result.categories.join(","))
            const updatedUrl = `${window.location.origin}${window.location.pathname}?${urlParams.toString()}`;
            history.pushState({}, "", updatedUrl );
            return result

        }

        if(catNormalizedArr.length) {
            //console.log(catNormalizedArr.join(""), catalogFilter.categories.join(""))
            if(catNormalizedArr.join("") !== catalogFilter.categories.join("")) {
                setCatalogFilter(catalogFilter.search, catNormalizedArr)
            }
        }
        const addProduct = async (e, item) => {
            e.preventDefault();
            const cartData = await getCartData()
            const productId = item.getAttribute('data-id');
            const alreadyInCart = isProductInCart(productId, cartData)

            if (!alreadyInCart) {
                const added = await addToCart(productId, cartData);

                if (added) {
                    renderAdded(item)
                }
                return;
            }

            renderAdded(item)

        };

        const toggleEmptyCatalog = (isEmpty) => {
            const ctgBlock = document.getElementById("catalog-a-block")
            const ctgEmptyBlock = document.getElementById("catalog-empty")

            if (isEmpty) {
                ctgBlock.style.display = "none"
                ctgEmptyBlock.style.display = "flex"
                return;
            }

            ctgBlock.style.display = "flex"
            ctgEmptyBlock.style.display = "none"
        }

        const handleChangeCategory = async (checkbox, categoryId) => {
            checkbox.checked = !checkbox.checked;
            catalogPart = 1
            catalogDesktopList.innerHTML = "<p style=\"font-size: 1.6rem; color: orange\">Ожидайте...</p>"
            const catalogFilterTemp = JSON.parse(JSON.stringify(catalogFilter))

            if (checkbox.checked) {
                if(!catalogFilterTemp.categories.some(id => id === categoryId)) {
                    catalogFilterTemp.categories.push(categoryId);

                }


            } else {
                const index = catalogFilterTemp.categories.indexOf(categoryId);

                if (index !== -1) {


                    catalogFilterTemp.categories = catalogFilterTemp.categories.filter(item => item !== categoryId);

                }
            }
            const changedFilter = setCatalogFilter(catalogFilterTemp.search, catalogFilterTemp.categories)


            renderProductsAndCategories(changedFilter)
                .catch(e => console.log(e)).then(() => {
                initCategoriesCheckboxes(true)
            })
        };

        if (catalogFilter.search.length) {

            catalogSearchBlocks.forEach(item => {
                item.value = catalogFilter.search
            })
        }


        const renderProductsAndCategories = async (filter = catalogFilter, resetRenderNeeded = true) => {

            const productsRes = await getProductsByCategory({
                analiz: filter.search,
                category: filter.categories,
                pag: catalogPart
            })
            const cartData = await getCartData(true)

            if (productsRes) {
                const analysis = productsRes.analiz
                const categories = productsRes.category

                renderCategories(categories);


                isCanLoaded = productsRes?.next_pag
                if (!isCanLoaded) {

                    loadBtn.style.display = "none"
                } else {
                    loadBtn.style.display = "block"
                }

                if (analysis.length) {

                    toggleEmptyCatalog(false)

                    if (resetRenderNeeded) {
                        catalogDesktopList.innerHTML = ""
                    }

                    analysis.forEach(product => {
                        const alreadyInCart = isProductInCart(product.id, cartData)
                        catalogDesktopList.innerHTML += CatalogProductItem(product, alreadyInCart)
                    })

                    const addToCartBtns = document.querySelectorAll(".product__a_item_btn");

                    addToCartBtns.forEach((item) => {
                        item.onclick = e => addProduct(e, item)
                    });

                    return;
                }
                toggleEmptyCatalog(true)

            }
            return productsRes
        }


        const renderCategories = (categories = []) => {
            categoriesDesktopBlocks.forEach(item => {
                item.innerHTML = "";
                categories.forEach(category => {
                    item.innerHTML += CategoryItem(category);
                });
            })
        };

        const initCategoriesCheckboxes = (checkboxRender = false) => {
            categoriesDesktopBlocks.forEach(item => {
                const ctgItems = item.querySelectorAll('.ctg__group_item');

                ctgItems.forEach(item => {
                    const categoryId = Number(item.dataset.ctg);
                    const checkbox = item.querySelector('.ctg__item_checkbox');


                    const categorySelected = JSON.parse(getCookie("catalogFilter")).categories.includes(categoryId)

                    if (categorySelected) {
                        checkbox.checked = true;
                    }



                    item.onclick = () => handleChangeCategory(checkbox, categoryId)
                });
            })
        }
        const initCatalog = () => {
            loadBtn.style.display = "none"
            renderProductsAndCategories().then(() => {
                initCategoriesCheckboxes(true)
            })
            const savedFilter = getCookie("catalogFilter");

            if (savedFilter) {
                const parsedFilter = JSON.parse(savedFilter);
                setCatalogFilter(parsedFilter.search, catNormalizedArr.length ? catNormalizedArr : parsedFilter.categories, false)
            } else {
                setCatalogFilter("", catNormalizedArr)
            }


            catalogSearchBlocks.forEach(catalogSearch => {
                catalogSearch.addEventListener('input', (e) => {
                    const searchValue = e.target.value
                    const ctgFilter = setCatalogFilter(searchValue, JSON.parse(getCookie('catalogFilter')).categories)

                    renderProductsAndCategories(ctgFilter)
                        .catch(e => console.log(e)).then(() => {
                        initCategoriesCheckboxes()
                    })
                })
            })
            loadBtn.onclick = () => {
                catalogPart += 1
                renderProductsAndCategories(catalogFilter, false)
                    .catch(e => console.log(e)).then(() => {
                    initCategoriesCheckboxes()
                })
            }
        }

        initCatalog()

    }
};

export default Catalog;

