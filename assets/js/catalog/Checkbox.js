const ctgItems = document.querySelectorAll('.ctg__group_item');

ctgItems.forEach(item => {

    const checkbox = item.querySelector('.ctg__item_checkbox');

    item.addEventListener('click', () => {

        checkbox.checked = !checkbox.checked;
    });
});