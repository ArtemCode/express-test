const pointMenuBtn = document.getElementById("point-menu__btn")
const closeMenuBtn = document.getElementById("cp__trigger_close")
const pointMenuBlock = document.getElementById("cp-block")
const openedClass = "cp__block-opened"

const pointMenuToggle = () => {
    if (!pointMenuBlock.classList.contains(openedClass)) {
        pointMenuBlock.classList.add(openedClass)
    } else {
        pointMenuBlock.classList.remove(openedClass)
    }
}

if(pointMenuBtn) {
    pointMenuBtn.onclick = pointMenuToggle
}

document.onclick = (e) => {
    if(pointMenuBlock) {
        if (pointMenuBlock.classList.contains(openedClass)) {
            if(!e.target.closest('.catalog__point_main') && !e.target.closest(".cp__block") || e.target.classList.contains("cp__trigger")) {
                pointMenuToggle()
            }

        }
    }

}
if(closeMenuBtn) {
    closeMenuBtn.onclick = pointMenuToggle
}


//pointMenuBlock.onclick = (e) => e.stopPropagation()