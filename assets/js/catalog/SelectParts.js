// Получаем все элементы ctg part
const ctgParts = document.querySelectorAll('.ctg__part');

// Добавляем обработчик события клика на каждый ctg part
ctgParts.forEach(part => {
    part.addEventListener('click', () => {
        // Удаляем класс selected у всех ctg part
        ctgParts.forEach(part => {
            part.classList.remove('ctg__part_selected');
        });

        // Добавляем класс selected к выбранному ctg part
        part.classList.add('ctg__part_selected');
    });
});
