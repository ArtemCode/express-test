// Загружаем данные корзины из куки
import {getCookie, setCookie} from '/assets/js/common/Cookier.js';
import {getProductById} from "/assets/js/queries/ProductsQueries.js";
import CartItem from "/assets/js/components/CartItem.js";
import {getAuthStatus} from "/assets/js/queries/AuthQueries.js";
import {cartAdd, cartDelete, getCart} from "/assets/js/queries/CartQueries.js";
import getPageId from "/assets/js/utils/getPageId.js";


const loadCartData = () => {
    const cartDataJSON = getCookie("cartData");
    if (cartDataJSON) {
        return JSON.parse(cartDataJSON);
    }
    return [];
};

const getCartData = (neededFromCache = false) => {
    return new Promise(async (resolve, reject) => {
        try {
            const {user_id} = await getAuthStatus();
            if (!user_id) {
                const loadedCart = loadCartData()
                resolve(loadedCart);
            } else {
                const cart = await getCart(neededFromCache);
                resolve(cart);
            }
        } catch (error) {
            reject(error);
        }
    });
};

const renderCartCount = (count) => {
    const countBlocks = document.querySelectorAll(".header__cart_count")
    countBlocks.forEach(item => {
        item.textContent = count
    })

};

const renderPrice = (totalCost) => {
    const pricesBlocks = document.querySelectorAll(".total-price")
    pricesBlocks.forEach(item => {
        item.textContent = totalCost
    })
}

const renderAdded = (item) => {
    item.children[0].innerHTML = `<svg  width="10" height="10" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" version="1.1" fill="none" stroke="#4D4D4D" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5">
                                        <polyline points="2.75 8.75,6.25 12.25,13.25 4.75"/>
                                    </svg>`
    item.children[1].innerHTML = "Добавлен"
}

const getTotalCount = (cartData = []) => {
    if (cartData || cartData?.length !== 0) {
        return cartData.reduce((count, item) => count + item.count, 0);
    }
    return 0

}

const getTotalCost = (cartData = []) => {
    if (cartData || cartData?.length !== 0) {
        const totalCost = cartData.reduce((acc, item) => acc + item.cost * item.count, 0);
        return totalCost;
    }
    return 0

}

const syncCart = async () => {
    const auth = await getAuthStatus();

    if (auth.user_id) {
        const cartData = await getCartData();
        const cookieCart = loadCartData();

        if (cookieCart) {
            const cookieCartIds = cookieCart.map(item => {
                return item.id
            })
            const serverCartIds = cartData.map(item => {
                return item.id
            })
            const localCartIsBigger = cookieCartIds.length > serverCartIds.length

            if (localCartIsBigger) {
                const neededCartIds = cookieCartIds.filter(prodId => !serverCartIds.some(item => item === prodId))
                await cartAdd(neededCartIds);
            }
        }
    }
}


    const getProductsIDS = (cartData = []) => {
        return cartData.map(product => Number(product.id))
    }

    const renderCartItems = (cartData = []) => {
        const cartProductsBlock = document.getElementById("cart-products")
        if (cartProductsBlock) {
            if (cartData.length) {
                cartProductsBlock.innerHTML = ""
                cartData.forEach((product) => {
                    cartProductsBlock.innerHTML += CartItem(product)
                })

                const allRemoveBtns = cartProductsBlock.querySelectorAll(".product__a_item_btn")

                allRemoveBtns.forEach(item => {
                    item.onclick = async (e) => {
                        const productId = e.currentTarget.getAttribute("data-del-id")

                        await removeFromCart(productId)
                    }
                })
                return;
            }
            cartProductsBlock.innerHTML = ` <div class="cart-empty w-100p f-center-row">
                        <h2>Корзина пуста.</h2>
                    </div>`

        }

    }


    const renderInfoCart = (cartData) => {
        const totalPrice = getTotalCost(cartData)
        const totalCount = getTotalCount(cartData)

        renderPrice(totalPrice)
        renderCartCount(totalCount)
        renderCartItems(cartData)
    }

    const saveCartData = (cartData) => {
        const cartDataJSON = JSON.stringify(cartData);
        setCookie("cartData", cartDataJSON, 7); // Здесь "7" - количество дней, на которое сохраняется куки
    };


    const updateCartData = (newCartData) => {
        saveCartData(newCartData);
        renderInfoCart(newCartData)
    };

    const isProductInCart = (productId, cartData = []) => {
        return cartData.some(item => item.id === Number(productId));
    };

    const addToCart = async (itemId, prevCart = []) => {
        const auth = await getAuthStatus()
        if (!auth.user_id) {

            const isExists = isProductInCart(itemId, prevCart)
            if (!isExists) {
                const cartDataRes = await getProductById(itemId);

                if (cartDataRes.status) {

                    const newProduct = {
                        id: cartDataRes.analiz.id,
                        cat: cartDataRes.analiz.cat,
                        code: cartDataRes.analiz.code,
                        cost: cartDataRes.analiz.cost,
                        name: cartDataRes.analiz.name,
                        count: 1
                    };
                    getCartData()
                        .then(cartData => {
                            cartData.push(newProduct);
                            updateCartData(cartData);
                            return newProduct;
                        })
                        .catch(er => console.log(er))
                    return itemId
                }
            }

        } else {
            return cartAdd([Number(itemId)])
                .then(async () => {
                    const cartData = await getCartData()
                    updateCartData(cartData)
                    return itemId
                })

        }


    };


    const removeFromCart = async (index) => {
        const auth = await getAuthStatus()
        const cartData = await getCartData(true);
        const newCartData = cartData.filter(item => item.id !== Number(index))

        if (!auth.user_id) {
            updateCartData(newCartData);
        } else {
            await cartDelete(Number(index))
            const newCart = await getCartData();
            updateCartData(newCart)
            renderInfoCart(newCart)
        }


    };

    const clearCart = () => {
        updateCartData([]);
    };

    const redirectEmptyCart = (pageId, cartData = []) => {
        if (pageId == "6") {
            if (!cartData.length) {
                window.location.href = "cart.html"
            }
        }
    }
    const initCartData = async (cb) => {
        try {
            await syncCart()
            getCartData()
                .then(async cartData => {

                    const pageId = getPageId()
                    redirectEmptyCart(pageId, cartData)

                    if (!cartData) {
                        updateCartData([])
                    }
                    if (cartData) {

                        renderInfoCart(cartData)

                    }
                    cb()
                })
        } catch (e) {
            console.log(e)
        }

    }


    export {
        loadCartData,
        clearCart,
        addToCart,
        getCartData,
        removeFromCart,
        saveCartData,
        updateCartData,
        isProductInCart,
        getProductsIDS,
        renderAdded,
        initCartData
    }
