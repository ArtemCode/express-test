function getMaxDaysInMonth(month, year) {
    const monthNum = Number(month);
    const yearNum = Number(year);

    if (monthNum === 2) {
        // Февраль
        return (yearNum % 4 === 0 && (yearNum % 100 !== 0 || yearNum % 400 === 0)) ? 29 : 28;
    } else if ([4, 6, 9, 11].includes(monthNum)) {
        // Апрель, Июнь, Сентябрь, Ноябрь
        return 30;
    } else {
        // Все остальные месяцы
        return 31;
    }
}

function correctDate(dateString) {
    const [day, month, year] = dateString.split(".");
    const correctedDay = Math.min(getMaxDaysInMonth(month, year), Math.max(1, Number(day))); // Ограничение дня в зависимости от месяца
    const correctedMonth = Math.min(12, Math.max(1, Number(month))); // Ограничение месяца от 1 до 12
    const correctedYear = Number(year);

    const date = new Date(correctedYear, correctedMonth - 1, correctedDay); // -1, потому что в JavaScript месяцы нумеруются с 0 до 11

    // Форматирование даты в виде "дд.мм.гггг"
    const formattedDate = `${String(date.getDate()).padStart(2, "0")}.${String(date.getMonth() + 1).padStart(2, "0")}.${date.getFullYear()}`;

    return formattedDate;
}

export const customDatepicker = (selector, parent, beforeShowDay = null, isBirthDay = false) => {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const minYear = isBirthDay ? currentYear - 100 : 0; // Пример: 10 лет назад
    const maxYear = currentYear; // Текущий год

    $(function () {
        $.datepicker.regional['ru'] = {
            minDate: isBirthDay ? '-100Y' : 0,
            appendTo: parent,
            closeText: 'Закрыть',
            prevText: '<',
            nextText: '>',
            currentText: 'Сегодня',
            allowTimes: ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'],
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekHeader: 'Нед',
            dateFormat: 'dd.mm.yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            showOtherMonths: true, // Показывать дни прошедших и будущих месяцев
            selectOtherMonths: true, //
            yearSuffix: '',
            changeMonth: true,
            changeYear: isBirthDay,
            yearRange: minYear + ":" + maxYear,
            beforeShowDay: beforeShowDay,
            beforeShow: function (input, inst) {
                // Вставка ui-datepicker-div в нужное место
                $(parent).append(inst.dpDiv);
            },
            onSelect: function (event) {
                const $input = $(this);
                const inputDate = $input.val();

                if (inputDate.length === 10) {
                    const isValidDate = $.datepicker.formatDate("dd.mm.yy", $input.datepicker("getDate")) === inputDate;

                    if (!isValidDate) {
                        $input.val("");
                    }
                }
            }
        };
        $(`${selector}`).datepicker($.datepicker.regional["ru"])
        const currentDate = new Date();
        const mask = isBirthDay ? "99.99.9999" : `99.99.${currentDate.getFullYear()}`
        Inputmask({
            mask: mask,
            placeholder: "дд.мм.гггг",
        }).mask($(selector));

        $(selector).on("input", function (event) {
            const inputValue = event.target.value

            if(!inputValue.includes("м") && !inputValue.includes("д") && !inputValue.includes("г")) {
                event.target.value = correctDate(inputValue)
            }

        });

        $.datepicker.setDefaults($.datepicker.regional['ru']);
    });
};


const DatePicker = {
    init() {
        customDatepicker("#input-datepicker", ".form-date-field");
    },
    profileInit() {
        customDatepicker("#profile-datepicker", ".form-date-field")
    },
    orderBirthdayInit() {
        customDatepicker("#input-db-datepicker", ".form-db-field", null, 1)
    },
    registerInit() {
        customDatepicker("#register-datepicker", ".register-date-field", null, 1)
    }

};

export default DatePicker;
