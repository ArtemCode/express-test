import {getFeedbackQuery, sendFeedbackQuery} from "/assets/js/queries/FeedbackQuery.js";
import getPageId from "/assets/js/utils/getPageId.js";
import HeaderFormField from "/assets/js/components/HeaderFormField.js";
import maskPhone from "/assets/js/common/PhoneMask.js";
import {applyInvalidStyles, formValidator} from "/assets/js/common/FormValidator.js";
import getNumberPhone from "/assets/js/utils/getNumberPhone.js";
import removePostfixFromFieldTypes from "/assets/js/utils/removePostfixForm.js";
import {modals} from "/assets/js/common/InitModals.js";

const Feedback = {

    init: () => {
        const pageID = getPageId();
        const headerFbBtn = document.getElementById("header-fb-send");
        const feedbackList = document.getElementById("header-fb");

        const renderForm = (res) => {

            if (res?.fields) {
                const {name, description, fields} = res;
                const feedbackTitle = document.getElementById("header_fb-title");
                const feedbackDescription = document.getElementById("header_fb_descr");

                feedbackTitle.textContent = name;
                feedbackDescription.textContent = description;
                feedbackList.innerHTML = "";

                fields.filter(item => item.type_field !== "none" && item.type_field !== "file").map(item => {
                    feedbackList.innerHTML += HeaderFormField(item);
                });

                const phoneSelector = '#header-feedback [data-field="phone"]';
                maskPhone(phoneSelector);
            }
        }

        getFeedbackQuery(pageID)
            .then((res) => {
                renderForm(res)
                return res
            })
            .then((res) => {
                if(!res?.fields) {
                    getFeedbackQuery(1)
                        .then((res) => {
                            renderForm(res)
                        })
                }
                const handleSendFeedback = async () => {
                    const fbFields = document.querySelectorAll("#header-fb .form__field_input");

                    const fbBody = {
                        form_id: res?.fields ? pageID : 1,
                        fields: []
                    };
                    const fieldCounts = {};

                    fbFields.forEach(item => {

                        let fieldType = item.dataset.field;
                        const isPhone = fieldType === "phone";
                        const fieldValue = isPhone ? getNumberPhone(item.value) : item.value;

                        if (fieldCounts[fieldType]) {
                            fieldCounts[fieldType]++;

                            const allDuplicates = document.querySelectorAll(`#header-feedback [data-field="${fieldType}"]`);
                            const postfix = `_${fieldCounts[fieldType]}`;
                            const newFieldType = `${fieldType}${postfix}`;

                            allDuplicates[fieldCounts[fieldType] - 1].setAttribute('data-field', newFieldType);

                            fbBody.fields.push({
                                type: newFieldType,
                                data: fieldValue
                            });
                        } else {
                            fieldCounts[fieldType] = 1;
                            fbBody.fields.push({
                                type: fieldType,
                                data: fieldValue
                            });
                        }
                    });

                    // Валидация формы
                    const validationRules = {};

                    fbFields.forEach(item => {

                        const fieldType = item.dataset.field;

                        let rules = {required: true, label: fieldType};

                        if (fieldType === "phone") {
                            rules.phone = true;
                        }
                        if (fieldType === "email") {
                            rules.email = true;
                        }

                        validationRules[fieldType] = rules;
                    });


                    const invalidFieldSelectors = ["header .form__field_input"];
                    const validationErrors = formValidator(fbBody.fields, validationRules, invalidFieldSelectors, true);

                    if (validationErrors.length > 0) {
                        // Если есть ошибки валидации, обработайте их здесь
                        // Например, выведите сообщения об ошибках или примените стили к невалидным полям
                        validationErrors.forEach(error => {
                            const {field, error: errorMessage} = error;
                            // Обработка ошибок валидации
                            applyInvalidStyles(invalidFieldSelectors, field, errorMessage);
                        });
                        return;
                    }

                    fbBody.fields = fbBody.fields.map(item => {
                        return removePostfixFromFieldTypes(item)
                    })
                    const {status} = await sendFeedbackQuery({...fbBody});

                    if (status) {


                        modals.get("header_form").close()
                        fbFields.forEach(item => {
                            item.value = ""
                        })
                    }
                };
                headerFbBtn.onclick = handleSendFeedback;
            });

    }
};
export default Feedback;
