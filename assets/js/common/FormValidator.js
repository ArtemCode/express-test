function isValidDate(dateString) {
    dateString = dateString?.includes(":") ? dateString.split(" ")[0] : dateString
    if (!/^\d{4}\-\d{2}\-\d{2}$/.test(dateString)) {
        return false;
    }

    return true;
}

function formValidator(formData, validationRules, invalidFieldSelectors, isFeedback = false) {
    const validationErrors = [];
    clearFields(invalidFieldSelectors);

    if(isFeedback) {

        for (const item of formData) {
            let field = item.type;
            const value = item.data;

            if (validationRules[field]) {
                const rules = validationRules[field];


                if (rules.required && !value) {

                    validationErrors.push({
                        field,
                        error: `Поле должно быть заполнено.`
                    });
                    applyInvalidStyles(invalidFieldSelectors, field, 'Поле заполнено некорректно.');
                }
                if (rules.phone && value) {
                    if (!/^\+?\d{10,15}$/.test(value) || !String(value).length === 11) {
                        validationErrors.push({field, error: `Некорректный формат телефона в поле "${rules.label}".`});

                        applyInvalidStyles(invalidFieldSelectors, field, 'Поле заполнено некорректно.'); // Применение стилей при невалидности
                    }
                }
                if (rules.date && value) {
                    if (!isValidDate(value)) {
                        validationErrors.push({field, error: `Некорректный формат телефона в поле "${rules.label}".`});

                        applyInvalidStyles(invalidFieldSelectors, field, 'Поле заполнено некорректно.'); // Применение стилей при невалидности
                    }
                }
                if (rules.email && value) {
                    if (!/^[\w.-]+@[a-zA-Z_-]+?(?:\.[a-zA-Z]{2,})+$/.test(value)) {
                        validationErrors.push({
                            field,
                            error: `Некорректный формат email в поле "${rules.label}".`
                        });
                        applyInvalidStyles(invalidFieldSelectors, field, 'Поле заполнено некорректно.');
                    }
                }
            }
        }
    } else {
        for (const field in validationRules) {
            if (Object.prototype.hasOwnProperty.call(validationRules, field)) {
                const rules = validationRules[field];
                if (rules.required) {

                    if (!formData[field]) {
                        validationErrors.push({field, error: `Поле "${rules.label}" должно быть заполнено.`});
                        applyInvalidStyles(invalidFieldSelectors, field, 'Поле заполнено некорректно.');
                    }
                    if (rules.phone) {
                        const phoneNumber = formData[field]

                        if (!/^\+?\d{10,15}$/.test(phoneNumber) || !String(phoneNumber).length === 11) {
                            validationErrors.push({field, error: `Некорректный формат телефона в поле "${rules.label}".`});

                            applyInvalidStyles(invalidFieldSelectors, field, 'Поле заполнено некорректно.'); // Применение стилей при невалидности
                        }
                    }
                    if (rules.date) {
                        if (!isValidDate(formData[field])) {
                            validationErrors.push({field, error: `Некорректный формат телефона в поле "${rules.label}".`});

                            applyInvalidStyles(invalidFieldSelectors, field, 'Поле заполнено некорректно.'); // Применение стилей при невалидности
                        }
                    }
                    if (rules.email) {
                        if (!/^[\w.-]+@[a-zA-Z_-]+?(?:\.[a-zA-Z]{2,})+$/.test(formData[field])) {
                            validationErrors.push({field, error: `Некорректный формат email в поле "${rules.label}".`});
                            applyInvalidStyles(invalidFieldSelectors, field, 'Поле заполнено некорректно.'); // Применение стилей при невалидности
                        }
                    }
                }

            }
        }
    }


    return validationErrors;
}

function clearFields(invalidFieldSelectors) {

    invalidFieldSelectors.forEach(selector => {
        document.querySelectorAll(selector).forEach(element => {

            element.classList.remove('form__field_invalid_caption');
            const invalidCaption = element.parentNode.nextElementSibling;
            if (invalidCaption && invalidCaption.classList.contains('form__field_invalid_caption')) {
                invalidCaption.textContent = ''; // Очистка текста невалидной подписи
            }
            const formFieldWrapper = element.closest('.form__field_wrapper');
            if (formFieldWrapper) {
                formFieldWrapper.classList.remove('red-border'); // Удаление класса с красной рамкой
            }
        });
    });
}

function applyInvalidStyles(selectors, field, message) {
    selectors.forEach(selector => {
        const inputElement = document.querySelector(`${selector}[data-field="${field}"]`);
        if (inputElement) {

            const invalidCaption = inputElement.parentNode.nextElementSibling;
            if (invalidCaption && invalidCaption.classList.contains('form__field_invalid_caption')) {
                invalidCaption.textContent = message; // Установка пользовательского текста невалидной подписи
                if (message) {
                    invalidCaption.classList.add('form__field_invalid_caption'); // Применение класса с стилями невалидного поля
                }
            }

            const formFieldWrapper = inputElement.closest('.form__field_wrapper');

            if (formFieldWrapper) {
                if (message) {
                    formFieldWrapper.classList.add('red-border'); // Добавление класса с красной рамкой
                } else {
                    formFieldWrapper.classList.remove('red-border'); // Удаление класса красной рамки в случае корректного формата
                }
            }
        }
    });
}

export default formValidator;
export {formValidator, applyInvalidStyles, clearFields}
