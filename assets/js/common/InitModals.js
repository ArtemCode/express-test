import {Modal} from "/assets/js/common/Modal.js";
import Search from "/assets/js/common/Search.js";

export const modals = new Map([
    [
        'mobile_menu',
        new Modal({
            triggersIds: ["trigger-mb-menu"],
            closeIds: ["menu-close"],
            elementId: "menu-mobile",
            switchableClass: "menu-mobile-opened",
        })
    ],
    [
        'mobile_filter',
        new Modal({
            triggersIds: ["filter-menu__btn"],
            closeIds: ["filter-menu__close", "catalog-fw-accept"],
            elementId: "catalog__fw_block",
            switchableClass: "catalog__fw-opened",
        })
    ],
    [
        'header_search',
        new Modal({
            triggersIds: ["header-input", "header-input-mobile"],
            elementId: "modal-search",
            switchableClass: "search__modal-opened",
            elementIsCloser: true,
            elNotPropagationId: "modal-search-content",
            onopen: () => {
                Search.resetHeaderInput()
            }
        })
    ],
    [
        'main_search',
        new Modal({
            triggersIds: ["main-search-btn"],
            closeIds: ["back-shadow"],
            elementId: "get-results__search",
            switchableClass: "search__modal-opened",
            onopen: () => {
                Search.resetMainInput()
                const shadowBlock = document.querySelector("#back-shadow")
                shadowBlock.classList.add("search__modal-opened")

                const resultsChildren = document.getElementById("get-results__search").children.length

                if(resultsChildren) {
                    const mainSearchField = document.getElementById("main-search-btn")
                    mainSearchField.classList.remove('search-fully-bordered')
                }
            },
            onclose: () => {
                const shadowBlock = document.querySelector("#back-shadow")
                shadowBlock.classList.remove("search__modal-opened")
                const mainSearchField = document.getElementById("main-search-btn")
                mainSearchField.classList.add('search-fully-bordered')
            }
        })
    ],
    [
        'header_form',
        new Modal({
            triggersIds: ["header__btn_form"],
            elementId: "header-feedback",
            switchableClass: "header__feedback-opened",
        })
    ],
        [
        'doctor_order',
            new Modal({
                triggersClass: "profile__btn",
                triggersClassNeeded: true,
                elementId: "modal-do",
                switchableClass: "modal-do-opened",
                elementIsCloser: true,
                elNotPropagationId: "modal-do__content"
            })
        ],
]);

const Modals = {
    init: function () {


        const commonSimpleModals = ['mobile_menu',  'mobile_filter',  'main_search'];
        const triggersSimpleModals = [
            'header_search',
            'header_form',
            'doctor_order'
        ];

        commonSimpleModals.forEach(modalName =>
            modals.get(modalName).defaultInit()
        );

        triggersSimpleModals.forEach(modalName => {
            modals.get(modalName).toggleInit();
        });

    }
}
export default Modals
