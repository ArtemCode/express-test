const InputsState = {
    init() {
        const allInputsNeededBorder = document.querySelectorAll(".bordered-input")

        allInputsNeededBorder.forEach(item => {
            const parent = item.parentElement
            const clearTextBtn = parent.querySelector(".input__clear") || null

            if (clearTextBtn) {
                clearTextBtn.onclick = (e) => {
                    const inputEvent = new Event('input', {
                        bubbles: true, // Событие всплывает
                        cancelable: true // Событие можно отменить
                    });
                    item.value = ""
                    item.dispatchEvent(inputEvent)
                    clearTextBtn.classList.add("v-hidden")
                }
                item.oninput = (e) => {
                    if (e.target.value.length) {
                        clearTextBtn.classList.remove("v-hidden")
                    } else {
                        clearTextBtn.classList.add("v-hidden")
                    }
                }
            }

            item.onfocus = () => {
                if(item.value.length) {
                    clearTextBtn.classList.remove("v-hidden")
                }

                parent.classList.add("block-yellow-bordered")

            }
            item.onblur = () => {
                parent.classList.remove("block-yellow-bordered")
            }
        })

    }
}
export default InputsState