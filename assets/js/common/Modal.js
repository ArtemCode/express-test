
export class Modal {
    constructor({
                    triggersIds = [],
                    triggersClass = "",
                    triggersClassNeeded = false,
                    closeIds = [],
                    elementId = "",
                    switchableClass = "",
                    elementIsCloser = false,
                    elNotPropagationId = "",
                    onopen = null,
                    onclose = null
                }) {


        this.triggerBtns = triggersIds.map(item => document.getElementById(item));
        this.triggerBtnByClass = triggersClassNeeded ? document.querySelectorAll(`.${triggersClass}`) : null
        this.closeBtns = closeIds.map(item => document.getElementById(item));
        this.modalElement = document.getElementById(elementId);
        this.switchableClass = switchableClass;
        this.elementId = elementId;
        this.triggersClassNeeded = triggersClassNeeded
        this.isOpened = false;
        this.elementIsCloser = elementIsCloser;
        this.elNotPropagationElement = document.getElementById(elNotPropagationId);
        this.onopen = onopen;
        this.onclose = onclose
    }

    open(e = this) {
        this.modalElement.classList.add(this.switchableClass);
        this.isOpened = true

        if (this.onopen) {
            this.onopen()
        }
    }

    close(e = this) {
        if (this.modalElement) {
            this.modalElement.classList.remove(this.switchableClass);
        }

        this.isOpened = false
        if (this.onclose) {
            this.onclose()
        }
    }

    toggle() {
        if (this.isOpened) {
            this.close()
            return;
        }
        this.open(this)
    }

    defaultInit() {
        if(this.triggersClassNeeded) {
            this.triggerBtnByClass.forEach(item => {
                item.onclick = this.open.bind(this)
            })
        } else {
            this.triggerBtns.forEach(trigger => {
                if(trigger) {
                    trigger.onclick = this.open.bind(this);
                }
            });
        }

        if (this.closeBtns.length) {
            this.closeBtns.forEach(closeBtn => {
                if(closeBtn) {
                    closeBtn.onclick = this.close.bind(this);
                }

            });
        }

    }


    toggleInit() {
        if(this.triggersClassNeeded) {

            this.triggerBtnByClass.forEach(item => {
                item.onclick = this.open.bind(this)
            })
        } else {
            this.triggerBtns.forEach(trigger => {
                trigger.onclick = function (e) {
                    const isNotModalBody = e.target.getAttribute('id') !== this.elementId;
                    if (isNotModalBody) {
                        this.toggle();
                    }
                }.bind(this);
            });
        }
        if (!this.elementIsCloser) {
            this.modalElement.onclick = e => e.stopPropagation();

            document.onclick = function (e) {
                if (this.isOpened) {
                    const isOutsideModal = !this.triggerBtns.some(trigger => e.target.closest("#" + trigger.id));

                    if (isOutsideModal) {
                        this.toggle();
                    }
                }
            }.bind(this);



            return;
        }

        if(this.modalElement) {
            this.modalElement.onclick = this.toggle.bind(this);
        }
        if(this.elNotPropagationElement) {
            this.elNotPropagationElement.onclick = e => e.stopPropagation();
        }


        if (this.closeBtns.length) {
            this.closeBtns.forEach(closeBtn => {
                closeBtn.onclick = this.close.bind(this);
            });
        }
    }

}
