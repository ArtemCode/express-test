function maskPhone(selector) {
    $(selector).inputmask('+7 (999) 999-99-99')
}

const phoneFieldMask  = () => {
    const phoneSelector = '[data-field="phone"]'
    maskPhone(phoneSelector)
}

export default maskPhone
export {phoneFieldMask}