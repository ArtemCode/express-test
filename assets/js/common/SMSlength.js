function limitInputLength(e) {
    if(e) {
        const maxLength = 6; // Максимальное количество символов

        if (e.target.value.length > maxLength) {
            e.target.value = e.target.value.slice(0, maxLength);
        }
    }

}
export default limitInputLength