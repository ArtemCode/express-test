import HeaderSearchProduct, {getIcon} from "/assets/js/components/HeaderSearch.js";
import {getProductsByName} from "/assets/js/queries/ProductsQueries.js";
import {addToCart, getCartData, isProductInCart} from "/assets/js/common/Cart.js";

let catalogPart = 1
let isCanLoaded = true
let isLoadedFirst = false
let isBlockedObserver = false
let inputValue = ""
let dispatchedHandle = false

const addCartSearchedProduct = async (e, item, searchResults, cartData) => {
    e.preventDefault();
    const productId = item.getAttribute('data-id');

    const isNotInCart = !isProductInCart(productId, cartData)

    if (isNotInCart) {
        const added = await addToCart(productId, cartData);
        if(added) {
            item.innerHTML = `
                ${getIcon(window.innerWidth <= 800, isNotInCart)}
                <p class="search__btn_wide">Добавлен</p>
            `
        }
        // if(searchResults.getAttribute('id') === "search-results-list") {
        //     Search.updateHeaderSearch()
        // } else {
        //     Search.updateMainSearch()
        // }

        item.querySelector(".already-in-cart").classList.remove('v-hidden');
    }
};

const renderSearchModalProducts = async (val, part, searchResults, endOfSearch,  resetNeeded = true, ) => {
    if(!isBlockedObserver) {
        isBlockedObserver = true

        const cartData = await getCartData(true)
        getProductsByName(val, part)
            .then((products = []) => {

                if(resetNeeded) {
                    searchResults.innerHTML = ""
                }

                const searchBlock = searchResults.parentElement.parentElement.children[0];

                searchBlock.classList.remove('search-fully-bordered');



                if (!isLoadedFirst) {
                    searchResults.innerHTML = "";
                    isLoadedFirst = true
                }

                if (!products.analiz.length) {

                    endOfSearch.style.display = "none"
                    searchResults.innerHTML = `<div class="search__not_found">Ничего не найдено</div>`;
                    return;
                }

                products.analiz.map((product) => {
                    const alreadyInCart = isProductInCart(product.id, cartData)
                    searchResults.innerHTML += HeaderSearchProduct(product, alreadyInCart);
                });


                const addToCartBtns = searchResults.querySelectorAll(".search__product_btn");
                addToCartBtns.forEach((item) => {
                    item.onclick = e => addCartSearchedProduct(
                        e,
                        item,
                        searchResults,
                        cartData
                    )
                });

                isCanLoaded = products.next_pag
                if(!isCanLoaded) {
                    endOfSearch.style.display = "none"
                } else {
                    endOfSearch.style.display = "block"
                }
            })
            .finally(() => {
                isBlockedObserver = false
            })

    }

}
const handleSearchInput = async (input, searchResults, endOfSearch) => {
    inputValue = input.value
    catalogPart = 1
    isCanLoaded = true
    isBlockedObserver = false


    await renderSearchModalProducts(inputValue,catalogPart , searchResults, endOfSearch)


}

const searchInit = (inputID, resultsID, searchEndID) => {
    const resultsInput = document.getElementById(inputID);
    const searchResults = document.getElementById(resultsID);
    const searchEnd = document.getElementById(searchEndID)
    //resultsInput.focus()
    resultsInput.addEventListener('input', async (e) => {

        await handleSearchInput(e.target, searchResults, searchEnd);

    });


    let options = {
        root: null, // viewport
        rootMargin: "0px",
        threshold: 0 // порог видимости элемента
    };

    let callback = function (entries, observer) {
        entries.forEach(entry => {

            if (entry.isIntersecting) {
                if(!isBlockedObserver) {

                    if (isLoadedFirst && isCanLoaded) {

                        catalogPart += 1
                        renderSearchModalProducts(inputValue, catalogPart, searchResults,  searchEnd, false).then()
                    }

                }

            }

        });
    };
    let observer = new IntersectionObserver(callback, options);
    observer.observe(searchEnd)

}

const Search = {
    init() {
        searchInit("search-input", "search-results-list", "search-results-anchor")
    },
    initMain() {
        searchInit("results-input", "get-results__search")
    },
    resetSearch(inputType = 0, prevValue = false) {
        const searchInputs = ["search-input", "results-input"]
        const currentInput = searchInputs[inputType] || null

        if(currentInput) {
            const resultsInput = document.getElementById(currentInput)
            resultsInput.value = prevValue ? resultsInput.value : ""
            const inputEvent = new Event('input');
            resultsInput.dispatchEvent(inputEvent);
        }
    },
    updateHeaderSearch() {
        this.resetSearch(0, true)
    },
    updateMainSearch() {
        this.resetSearch(1, true)
    },
    resetHeaderInput() {
        document.getElementById("search-input").focus()
        this.resetSearch(0)

    },
    resetMainInput() {
        this.resetSearch(1)
    }
};

export default Search;