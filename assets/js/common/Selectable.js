const SelectableFields = {
    init() {
        const selectableItems = document.querySelectorAll(".form__selectable");

        selectableItems.forEach(item => {
            item.addEventListener('click', () => {
                // Удаляем класс 'form__selectable-selected' у всех элементов
                selectableItems.forEach(item => item.classList.remove('form__selectable-selected'));

                // Добавляем класс 'form__selectable-selected' только выбранному элементу
                item.classList.add('form__selectable-selected');
            });
        });
    }
}
export default SelectableFields



