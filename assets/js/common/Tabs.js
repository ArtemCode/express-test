$(document).on("DOMContentLoaded", function () {

})
function initTabs(tabsSelector, tabContentsSelector, activeTab, activeContent) {
    const tabs = document.querySelectorAll(tabsSelector)
    tabs.forEach((item, index) => {
        item.setAttribute("data-tab", `${index}`)
    })
    const tabContents = document.querySelectorAll(tabContentsSelector)
    tabContents.forEach((item, index) => {
        item.setAttribute("id", `tab_${index}`)
    })

    const hideAllTabs = () => {
        tabContents.forEach(item => {
            item.classList.remove(activeContent)
        })
    }
    const disableActivesTabs = () => {
        tabs.forEach(item => {
            item.classList.remove(activeTab)
        })
    }
    tabs.forEach((tab) => {
        tab.onclick = (e) => {
            const tabId = tab.getAttribute('data-tab')
            hideAllTabs()
            const tabCurrent = document.querySelector(`#tab_${tabId}`)
            disableActivesTabs()
            tab.classList.add(activeTab)
            tabCurrent.classList.add(activeContent)

        }
    })
}

export default initTabs