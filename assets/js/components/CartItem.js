import {isProductInCart} from "/assets/js/common/Cart.js";

function CartItem (props = {
    name: "",
    code: "",
    cost: 0,
    id: 0

}) {

    return `
      <div class="product__a_item flex-column gap-10 w-100p white-bordered-shadow">
                <div class="product__a_item_top d-f js-between">
                    <div class="product__a_item_left">
                        <p class="product__a_item_text">${props.name}</p>
                    </div>
                    <div class="product__a_price">
                        <span>${props.cost}</span>&nbsp;₽
                    </div>
                </div>
                <div class="product__a_item_bottom flex-row-betw">
                    <div class="product__a_item_left flex-row-betw">
                        <p class="analyze__caption">1 календарный день</p>
                        <div class="product__a_type d-f al-center gap-10">
                            <p class="analyze__caption">Анализ</p>
                            <div class="analyze__code">
                                ${props.code}
                            </div>
                        </div>
                    </div>
                    <div data-del-id=${props.id}  class="product__a_item_btn  d-f al-center jc-center">
                      
                            <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 10 10" fill="none">
                                <path opacity="0.4" d="M8.05159 3.94507C8.05159 3.94507 7.82534 6.75132 7.69409 7.9334C7.63159 8.49798 7.28284 8.82882 6.71159 8.83923C5.62451 8.85882 4.53617 8.86007 3.44951 8.83715C2.89992 8.8259 2.55701 8.4909 2.49576 7.93632C2.36367 6.74382 2.13867 3.94507 2.13867 3.94507" stroke="#4D4D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M8.62833 2.59987H1.5625" stroke="#4D4D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M7.26674 2.59987C6.93966 2.59987 6.65799 2.36862 6.59383 2.0482L6.49258 1.54153C6.43008 1.30778 6.21841 1.14612 5.97716 1.14612H4.21341C3.97216 1.14612 3.76049 1.30778 3.69799 1.54153L3.59674 2.0482C3.53258 2.36862 3.25091 2.59987 2.92383 2.59987" stroke="#4D4D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                            

                            <p>
                                Удалить
                            </p>
                       
                    </div>
                </div>
            </div>
    `
}
export default CartItem