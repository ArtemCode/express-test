import {isProductInCart} from "/assets/js/common/Cart.js";

function CatalogProductItem(props = {
    name: "",
    code: "",
    cost: 0,
    id: 0

}, alreadyInCart) {

    const textBtn = alreadyInCart ? "Добавлен" : "В корзину"
    const isMobile = window.innerWidth <= 800
    const getIcon = () => {
        if (alreadyInCart) {
            return `<svg class="${isMobile ? "search__btn_mini" : ""}" width="10" height="10" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" version="1.1" fill="none" stroke="#4D4D4D" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5">
                            <polyline points="2.75 8.75,6.25 12.25,13.25 4.75"/>
                        </svg>`
        }

        return `  <svg width="10" height="10" viewBox="0 0 10 10" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd"
                      d="M6.88074 8.95841H3.40248C2.12482 8.95841 1.14466 8.49693 1.42307 6.63957L1.74725 4.12241C1.91887 3.19563 2.51003 2.84094 3.02871 2.84094H7.26975C7.79607 2.84094 8.35289 3.22233 8.55122 4.12241L8.8754 6.63957C9.11186 8.28717 8.15839 8.95841 6.88074 8.95841Z"
                      stroke="#4D4D4D" stroke-linecap="round" stroke-linejoin="round"/>
                <path opacity="0.4"
                   d="M6.93801 2.74939C6.93801 1.75519 6.13206 0.949235 5.13786 0.949235V0.949235C4.65911 0.947206 4.19927 1.13597 3.86002 1.47378C3.52078 1.8116 3.33007 2.27063 3.33008 2.74939H3.33008"
                      stroke="#4D4D4D" stroke-linecap="round" stroke-linejoin="round"/>
               <path d="M6.37357 4.62587H6.3545" stroke="#4D4D4D" stroke-linecap="round"
                     stroke-linejoin="round"/>
                <path d="M3.94388 4.62587H3.92481" stroke="#4D4D4D" stroke-linecap="round"
                      stroke-linejoin="round"/>
          </svg>`


    }

    return `
     <a href="/${props.id}" class="product__a_item w-100p white-bordered-shadow flex-column gap-10">
            <div class="product__a_item_top d-f js-between">
                <div class="product__a_item_left">
                    <p class="product__a_item_text">${props.name}</p>
                </div>
                <div class="product__a_price">
                    <span>${props.cost}</span>&nbsp;₽
                </div>
            </div>
            <div class="product__a_item_bottom flex-row-betw">
                <div class="product__a_item_left flex-row-betw">
                    <p class="analyze__caption">1 календарный день</p>
                    <div class="product__a_type d-f al-center gap-10">
                        <p class="analyze__caption">Анализ</p>
                        <div class="analyze__code">
                            ${props.code}
                        </div>
                    </div>
                </div>
                <div class="flex-column gap-5">
                     <div data-id="${props.id}" class="product__a_item_btn d-f al-center jc-center">
                      
                        ${getIcon()}
                        
                        <p>
                           ${textBtn}
                        </p>
    
    
                    </div>
                  
                </div>
               
            </div>
        </a>
    `
}

export default CatalogProductItem