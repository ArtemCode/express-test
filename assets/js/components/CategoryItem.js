const CategoryItem = (props ={
    id: 0,
    color: "",
    name: "",
    count: "-/-"
}) => {

    return `<div data-ctg="${props.id}" class="ctg__group_item d-f js-between">
        <p>${props.name}</p>
        <div class="ctg__item_right d-f al-center gap-15">
            <p>${props.count}</p>
            <input type="checkbox" class=" ctg__item_checkbox d-n">
                <div class="ctg__checkbox p-rel cur-pointer f-center-col">
                    <svg class="p-abs w-100p h-100p ctg__checkbox_icon" viewBox="0 0 17 17" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <g id="Ð§ÐµÐº">
                            <g id="ic_checkbox_active">
                                <path id="Fill" fill-rule="evenodd" clip-rule="evenodd"
                                      d="M3.4 0H13.6C15.4778 0 17 1.52223 17 3.4V13.6C17 15.4778 15.4778 17 13.6 17H3.4C1.52223 17 0 15.4778 0 13.6V3.4C0 1.52223 1.52223 0 3.4 0Z"
                                      fill="url(#paint0_linear_95_21271)"></path>
                                <path id="Path" fill-rule="evenodd" clip-rule="evenodd"
                                      d="M13.351 6.28361L7.86367 11.7712L7.86346 11.7715C7.61051 12.0244 7.22467 12.0639 6.93026 11.89C6.87564 11.8578 6.82431 11.8181 6.77742 11.7715C6.77732 11.7714 6.77732 11.7714 6.77732 11.7714L3.61785 8.61191C3.31814 8.31198 3.31814 7.8258 3.61785 7.52577C3.91778 7.22584 4.40406 7.22584 4.70399 7.52577L7.32045 10.1422L12.2651 5.19759C12.5649 4.89776 13.0513 4.89776 13.3511 5.19759C13.6509 5.49762 13.6509 5.98378 13.351 6.28361V6.28361Z"
                                      fill="white"></path>
                            </g>
                        </g>
                        <defs>
                            <linearGradient id="paint0_linear_95_21271" x1="-4.62577e-07" y1="17" x2="17"
                                            y2="1.56398e-06" gradientUnits="userSpaceOnUse">
                                <stop stop-color="#12B2B3"></stop>
                                <stop offset="1" stop-color="#56E0E0"></stop>
                            </linearGradient>
                        </defs>
                    </svg>

                </div>

        </div>
    </div>`
}
export default CategoryItem