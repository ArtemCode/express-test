function HeaderFormField(props = {
    type_field: "",
    text_in_field: "",
    text_ander_field: ""

}) {
    const getInputByType = (type) => {
        switch (type) {
            case "long_field":
                return `<textarea class="hf__input hf__textarea form__field_input bordered-input w-100p" placeholder='${props.text_in_field}' data-field=${props.type_field}></textarea>`

            default:

                return `<input required style="font-size: ${type === "file" ? "1.1rem" : "inherit"}" type=${type === "file" ? "text" : "text"} class="hf__input form__field_input bordered-input w-100p"
                              placeholder=${props.text_in_field} data-field="${type === "file" ? "short_field" : type}"/>`
        }
    }
    return `
        <div class="form__field hf__field">
            <div class="form__label hf__label">${props.text_ander_field}</div>
            <div class="form__field_wrapper input__block">${getInputByType(props.type_field)}</div>
            <p class="red-text form__field_invalid_caption"></p>
        </div>
    `
}

export default HeaderFormField