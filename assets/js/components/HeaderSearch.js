export const getIcon = (isMobile, alreadyInCart) => {
    if (alreadyInCart) {

        return `<svg class="${isMobile ? "search__btn_mini" : ""}" width="10" height="10" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" version="1.1" fill="none" stroke="#4D4D4D" stroke-linecap="round" stroke-linejoin="round" stroke-width="2.5">
                            <polyline points="2.75 8.75,6.25 12.25,13.25 4.75"/>
                        </svg>`
    }
    if (isMobile) {

        return `<svg class="search__btn_mini" width="10" height="10" viewBox="0 0 10 10" fill="black" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M5.75005 1.02124C5.75005 
                        0.607027 5.41427 0.27124 5.00005 0.27124C4.58584 0.27124 4.25005 0.607027 
                        4.25005 1.02124V4.23975H1.02783C0.613618 4.23975 0.277832 4.57553 0.277832 
                        4.98975C0.277832 5.40396 0.613618 5.73975 1.02783 
                        5.73975H4.25005V8.95813C4.25005 9.37234 4.58584 9.70813 5.00005 
                        9.70813C5.41427 9.70813 5.75005 9.37234 5.75005 8.95813V5.73975H8.97228C9.38649 
                        5.73975 9.72228 5.40396 9.72228 4.98975C9.72228 4.57553 9.38649 4.23975 
                        8.97228 4.23975H5.75005V1.02124Z" fill="#4D4D4D"/>
                    </svg>
                    `
    }

    return `  <svg class="search__btn_wide" width="10" height="10" viewBox="0 0 10 10" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                          d="M6.88074 8.95841H3.40248C2.12482 8.95841 1.14466 8.49693 1.42307 6.63957L1.74725 4.12241C1.91887 3.19563 2.51003 2.84094 3.02871 2.84094H7.26975C7.79607 2.84094 8.35289 3.22233 8.55122 4.12241L8.8754 6.63957C9.11186 8.28717 8.15839 8.95841 6.88074 8.95841Z"
                          stroke="#4D4D4D" stroke-linecap="round" stroke-linejoin="round"/>
                    <path opacity="0.4"
                       d="M6.93801 2.74939C6.93801 1.75519 6.13206 0.949235 5.13786 0.949235V0.949235C4.65911 0.947206 4.19927 1.13597 3.86002 1.47378C3.52078 1.8116 3.33007 2.27063 3.33008 2.74939H3.33008"
                          stroke="#4D4D4D" stroke-linecap="round" stroke-linejoin="round"/>
                   <path d="M6.37357 4.62587H6.3545" stroke="#4D4D4D" stroke-linecap="round"
                         stroke-linejoin="round"/>
                    <path d="M3.94388 4.62587H3.92481" stroke="#4D4D4D" stroke-linecap="round"
                          stroke-linejoin="round"/>
              </svg>`

}
function HeaderSearchProduct(props = {
    cat: 0,
    code: "",
    cost: 0,
    id: 0,
    info: "",
    name: "",
    prepare: []

}, alreadyInCart) {


    const textBtn = alreadyInCart ? "Добавлен" : "В корзину"
    const isMobile = window.innerWidth <= 800



    return `
        <a  href="/${props.id}" class="w-100p search__product flex-row-betw">
                <div class="search__product_left flex-column gap-5">
                    <div class="search__product_title">${props.name}</div>
                    <div class="search__product_days">1 календарный день</div>
                </div>
                <div class="search__product_right d-f al-center gap-15">
                    <div class="search__product_price">
                        <span>${props.cost}</span>&nbsp;₽
                    </div>
                    <div class="flex-column gap-5 ">
                        <div data-id="${props.id}" class="btn btn-yellow search__product_btn p-rel">
                            
                        
                            ${getIcon(isMobile, alreadyInCart)}
                            <p class="search__btn_wide">${textBtn}</p>
                            
                         
                            
                        </div>
                        
                        
                    </div>
</div>
                    
                </div>

            </a>
    `
}

export default HeaderSearchProduct