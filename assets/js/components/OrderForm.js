const OrderFormComponent = () => {
    return `
                <div class="order__left_top flex-column gap-20">
                    <h2 id="order-title" class="section__title fw-6">Оформление заказа</h2>
                    <div class="order__left_ways form__ways d-f">
                        <div class="form__type form__type-selected">Без авторизации</div>
                        <a href="" class="form__type ">Авторизоваться</a>
                    </div>
                </div>
                <div class="order__form flex-column gap-30">
                    <div class="form__field ">
                        <div class="form__label">Фамилия*</div>
                        <div class="form__field_wrapper input__block">
                            <input type="text" id="last-name" class="form__field_input bordered-input"
                                   placeholder="Иванов" data-field="last_name">
                        </div>
                        <p class="red-text form__field_invalid_caption"></p>
                    </div>
                    <div class="form__field ">
                        <div class="form__label">Имя*</div>
                        <div class="form__field_wrapper input__block">
                            <input type="text" id="first-name" class="form__field_input bordered-input"
                                   placeholder="Иван" data-field="first_name">
                        </div>
                        <p class="red-text form__field_invalid_caption"></p>
                    </div>
                    <div class="form__field ">
                        <div class="form__label">Отчество*</div>
                        <div class="form__field_wrapper input__block">
                            <input type="text" id="middle-name" class="form__field_input bordered-input"
                                   placeholder="Иванович" data-field="surname">
                        </div>
                        <p class="red-text form__field_invalid_caption"></p>
                    </div>
                    <div class="form__field">
                        <div class="form__label">Дата посещения*</div>
                        <label for="input-datepicker" class="form__field_wrapper input__block p-rel order-date form-date-field">
                            <input readonly type="text" id="input-datepicker" class="form__field_input bordered-input"
                                   placeholder="Выберите дату" data-field="dob">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <g id="&#208;&#154;&#208;&#176;&#209;&#128;&#209;&#130;&#208;&#190;&#209;&#135;&#208;&#186;&#208;&#176; &#209;&#130;&#208;&#190;&#208;&#178;&#208;&#176;&#209;&#128;&#208;&#176;/Iconly/Two-tone/Calendar">
                                    <g id="Calendar">
                                        <path id="Line_200" opacity="0.4" d="M3.0918 9.40427H20.9157" stroke="#4D4D4D"
                                              stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                        <path id="Union" opacity="0.4" fill-rule="evenodd" clip-rule="evenodd"
                                              d="M6.80859 13.3098C6.80859 12.8955 7.14438 12.5598 7.55859 12.5598H7.56786C7.98207 12.5598 8.31786 12.8955 8.31786 13.3098C8.31786 13.724 7.98207 14.0598 7.56786 14.0598H7.55859C7.14438 14.0598 6.80859 13.724 6.80859 13.3098ZM12.0053 12.5598C11.5911 12.5598 11.2553 12.8955 11.2553 13.3098C11.2553 13.724 11.5911 14.0598 12.0053 14.0598H12.0146C12.4288 14.0598 12.7646 13.724 12.7646 13.3098C12.7646 12.8955 12.4288 12.5598 12.0146 12.5598H12.0053ZM16.4428 12.5598C16.0286 12.5598 15.6928 12.8955 15.6928 13.3098C15.6928 13.724 16.0286 14.0598 16.4428 14.0598H16.452C16.8663 14.0598 17.202 13.724 17.202 13.3098C17.202 12.8955 16.8663 12.5598 16.452 12.5598H16.4428ZM16.4428 16.4462C16.0286 16.4462 15.6928 16.782 15.6928 17.1962C15.6928 17.6104 16.0286 17.9462 16.4428 17.9462H16.452C16.8663 17.9462 17.202 17.6104 17.202 17.1962C17.202 16.782 16.8663 16.4462 16.452 16.4462H16.4428ZM11.2553 17.1962C11.2553 16.782 11.5911 16.4462 12.0053 16.4462H12.0146C12.4288 16.4462 12.7646 16.782 12.7646 17.1962C12.7646 17.6104 12.4288 17.9462 12.0146 17.9462H12.0053C11.5911 17.9462 11.2553 17.6104 11.2553 17.1962ZM7.55859 16.4462C7.14438 16.4462 6.80859 16.782 6.80859 17.1962C6.80859 17.6104 7.14438 17.9462 7.55859 17.9462H7.56786C7.98207 17.9462 8.31786 17.6104 8.31786 17.1962C8.31786 16.782 7.98207 16.4462 7.56786 16.4462H7.55859Z"
                                              fill="#4D4D4D"/>
                                        <path id="Line_207" d="M16.0433 2V5.29078" stroke="#4D4D4D" stroke-width="1.5"
                                              stroke-linecap="round" stroke-linejoin="round"/>
                                        <path id="Line_208" d="M7.96515 2V5.29078" stroke="#4D4D4D" stroke-width="1.5"
                                              stroke-linecap="round" stroke-linejoin="round"/>
                                        <path id="Path" fill-rule="evenodd" clip-rule="evenodd"
                                              d="M16.2383 3.57922H7.77096C4.83427 3.57922 3 5.21516 3 8.22225V17.2719C3 20.3263 4.83427 22 7.77096 22H16.229C19.175 22 21 20.3546 21 17.3475V8.22225C21.0092 5.21516 19.1842 3.57922 16.2383 3.57922Z"
                                              stroke="#4D4D4D" stroke-width="1.5" stroke-linecap="round"
                                              stroke-linejoin="round"/>
                                    </g>
                                </g>
                            </svg>

                        </label>
                        <p class="red-text form__field_invalid_caption"></p>
                    </div>
                    
                    <div class="form__field">
                        <div class="form__label">Время посещения*</div>
                        <label for="order-time" class="form__field_wrapper input__block p-rel">
                            <input value="10:00" type="time" id="order-time" class="form__field_input bordered-input"
                                   placeholder="Выберите дату" data-field="time">
                        </label>
                        <p class="red-text form__field_invalid_caption"></p>
                    </div>
                    <div class="form__field">
                        <div class="form__label">Пол</div>
                        <div class="form__selectable_wrapper">

                            <div class="form__selectable form__selectable-selected" data-field="sex">Мужчина</div>
                            <div class="form__selectable" data-field="sex">Женщина</div>
                        </div>
                    </div>
                    <div class="form__field">
                        <div class="form__label">Дата рождения*</div>
                        <div class="form__field_wrapper input__block p-rel order-date form-db-field">
                            <input type="text" id="input-db-datepicker" class="form__field_input bordered-input"
                                   placeholder="Выберите дату" data-field="birthday">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <g id="&#208;&#154;&#208;&#176;&#209;&#128;&#209;&#130;&#208;&#190;&#209;&#135;&#208;&#186;&#208;&#176; &#209;&#130;&#208;&#190;&#208;&#178;&#208;&#176;&#209;&#128;&#208;&#176;/Iconly/Two-tone/Calendar">
                                    <g id="Calendar">
                                        <path id="Line_200" opacity="0.4" d="M3.0918 9.40427H20.9157" stroke="#4D4D4D"
                                              stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                        <path id="Union" opacity="0.4" fill-rule="evenodd" clip-rule="evenodd"
                                              d="M6.80859 13.3098C6.80859 12.8955 7.14438 12.5598 7.55859 12.5598H7.56786C7.98207 12.5598 8.31786 12.8955 8.31786 13.3098C8.31786 13.724 7.98207 14.0598 7.56786 14.0598H7.55859C7.14438 14.0598 6.80859 13.724 6.80859 13.3098ZM12.0053 12.5598C11.5911 12.5598 11.2553 12.8955 11.2553 13.3098C11.2553 13.724 11.5911 14.0598 12.0053 14.0598H12.0146C12.4288 14.0598 12.7646 13.724 12.7646 13.3098C12.7646 12.8955 12.4288 12.5598 12.0146 12.5598H12.0053ZM16.4428 12.5598C16.0286 12.5598 15.6928 12.8955 15.6928 13.3098C15.6928 13.724 16.0286 14.0598 16.4428 14.0598H16.452C16.8663 14.0598 17.202 13.724 17.202 13.3098C17.202 12.8955 16.8663 12.5598 16.452 12.5598H16.4428ZM16.4428 16.4462C16.0286 16.4462 15.6928 16.782 15.6928 17.1962C15.6928 17.6104 16.0286 17.9462 16.4428 17.9462H16.452C16.8663 17.9462 17.202 17.6104 17.202 17.1962C17.202 16.782 16.8663 16.4462 16.452 16.4462H16.4428ZM11.2553 17.1962C11.2553 16.782 11.5911 16.4462 12.0053 16.4462H12.0146C12.4288 16.4462 12.7646 16.782 12.7646 17.1962C12.7646 17.6104 12.4288 17.9462 12.0146 17.9462H12.0053C11.5911 17.9462 11.2553 17.6104 11.2553 17.1962ZM7.55859 16.4462C7.14438 16.4462 6.80859 16.782 6.80859 17.1962C6.80859 17.6104 7.14438 17.9462 7.55859 17.9462H7.56786C7.98207 17.9462 8.31786 17.6104 8.31786 17.1962C8.31786 16.782 7.98207 16.4462 7.56786 16.4462H7.55859Z"
                                              fill="#4D4D4D"/>
                                        <path id="Line_207" d="M16.0433 2V5.29078" stroke="#4D4D4D" stroke-width="1.5"
                                              stroke-linecap="round" stroke-linejoin="round"/>
                                        <path id="Line_208" d="M7.96515 2V5.29078" stroke="#4D4D4D" stroke-width="1.5"
                                              stroke-linecap="round" stroke-linejoin="round"/>
                                        <path id="Path" fill-rule="evenodd" clip-rule="evenodd"
                                              d="M16.2383 3.57922H7.77096C4.83427 3.57922 3 5.21516 3 8.22225V17.2719C3 20.3263 4.83427 22 7.77096 22H16.229C19.175 22 21 20.3546 21 17.3475V8.22225C21.0092 5.21516 19.1842 3.57922 16.2383 3.57922Z"
                                              stroke="#4D4D4D" stroke-width="1.5" stroke-linecap="round"
                                              stroke-linejoin="round"/>
                                    </g>
                                </g>
                            </svg>

                        </div>
                        <p class="red-text form__field_invalid_caption"></p>
                    </div>
                    <div class="form__field ">
                        <div class="form__label">Телефон*</div>
                        <div class="form__field_wrapper input__block">
                            <input data-field="phone" id="order-phone" type="text"
                                   class="form__field_input bordered-input" placeholder="+7">
                        </div>
                        <p class="red-text form__field_invalid_caption"></p>
                    </div>
                    <div class="form__field ">
                        <div class="form__label">E-mail*</div>
                        <div class="form__field_wrapper input__block">
                            <input data-field="email" id="email" type="email"
                                   class="form__field_input bordered-input" placeholder="pochta@mail.ru">
                        </div>
                        <p class="red-text form__field_invalid_caption"></p>
                    </div>
                </div>
           `
}
const AuthOrderFormComponent = (email) => {
    return  `<div class="order__left_top flex-column gap-20">
            <h2 id="order-title" class="section__title fw-6">Оформление заказа</h2>
            <div class="order__left_ways form__ways d-n">
                <div class="form__type form__type-selected">Без авторизации</div>
                <a href="" class="form__type ">Авторизоваться</a>
            </div>
        </div>
        <div class="order__form flex-column gap-30">
            <div class="form__field">
                <div class="form__label">Дата посещения*</div>
                <label for="input-datepicker" class="form__field_wrapper input__block p-rel order-date form-date-field">
                    <input type="text" id="input-datepicker" class="form__field_input bordered-input"
                           placeholder="Выберите дату" data-field="dob">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <g id="&#208;&#154;&#208;&#176;&#209;&#128;&#209;&#130;&#208;&#190;&#209;&#135;&#208;&#186;&#208;&#176; &#209;&#130;&#208;&#190;&#208;&#178;&#208;&#176;&#209;&#128;&#208;&#176;/Iconly/Two-tone/Calendar">
                                <g id="Calendar">
                                    <path id="Line_200" opacity="0.4" d="M3.0918 9.40427H20.9157" stroke="#4D4D4D"
                                          stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path id="Union" opacity="0.4" fill-rule="evenodd" clip-rule="evenodd"
                                          d="M6.80859 13.3098C6.80859 12.8955 7.14438 12.5598 7.55859 12.5598H7.56786C7.98207 12.5598 8.31786 12.8955 8.31786 13.3098C8.31786 13.724 7.98207 14.0598 7.56786 14.0598H7.55859C7.14438 14.0598 6.80859 13.724 6.80859 13.3098ZM12.0053 12.5598C11.5911 12.5598 11.2553 12.8955 11.2553 13.3098C11.2553 13.724 11.5911 14.0598 12.0053 14.0598H12.0146C12.4288 14.0598 12.7646 13.724 12.7646 13.3098C12.7646 12.8955 12.4288 12.5598 12.0146 12.5598H12.0053ZM16.4428 12.5598C16.0286 12.5598 15.6928 12.8955 15.6928 13.3098C15.6928 13.724 16.0286 14.0598 16.4428 14.0598H16.452C16.8663 14.0598 17.202 13.724 17.202 13.3098C17.202 12.8955 16.8663 12.5598 16.452 12.5598H16.4428ZM16.4428 16.4462C16.0286 16.4462 15.6928 16.782 15.6928 17.1962C15.6928 17.6104 16.0286 17.9462 16.4428 17.9462H16.452C16.8663 17.9462 17.202 17.6104 17.202 17.1962C17.202 16.782 16.8663 16.4462 16.452 16.4462H16.4428ZM11.2553 17.1962C11.2553 16.782 11.5911 16.4462 12.0053 16.4462H12.0146C12.4288 16.4462 12.7646 16.782 12.7646 17.1962C12.7646 17.6104 12.4288 17.9462 12.0146 17.9462H12.0053C11.5911 17.9462 11.2553 17.6104 11.2553 17.1962ZM7.55859 16.4462C7.14438 16.4462 6.80859 16.782 6.80859 17.1962C6.80859 17.6104 7.14438 17.9462 7.55859 17.9462H7.56786C7.98207 17.9462 8.31786 17.6104 8.31786 17.1962C8.31786 16.782 7.98207 16.4462 7.56786 16.4462H7.55859Z"
                                          fill="#4D4D4D"/>
                                    <path id="Line_207" d="M16.0433 2V5.29078" stroke="#4D4D4D" stroke-width="1.5"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                    <path id="Line_208" d="M7.96515 2V5.29078" stroke="#4D4D4D" stroke-width="1.5"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                    <path id="Path" fill-rule="evenodd" clip-rule="evenodd"
                                          d="M16.2383 3.57922H7.77096C4.83427 3.57922 3 5.21516 3 8.22225V17.2719C3 20.3263 4.83427 22 7.77096 22H16.229C19.175 22 21 20.3546 21 17.3475V8.22225C21.0092 5.21516 19.1842 3.57922 16.2383 3.57922Z"
                                          stroke="#4D4D4D" stroke-width="1.5" stroke-linecap="round"
                                          stroke-linejoin="round"/>
                                </g>
                            </g>
                        </svg>

                </label>
                <p class="red-text form__field_invalid_caption"></p>
            </div>
            <div class="form__field">
                <div class="form__label">Время посещения*</div>
                <div class="form__field_wrapper input__block p-rel">
                    <input value="10:00" type="time" id="order-time" class="form__field_input bordered-input"
                           placeholder="Выберите дату" data-field="time">
                </div>
                <p class="red-text form__field_invalid_caption"></p>
            </div>
            <div class="form__field">
                        <div class="form__label">E-mail*</div>
                        <div class="form__field_wrapper input__block">
                            <input readonly data-field="email" id="email" value="${email}" type="email"
                                   class="form__field_input bordered-input" placeholder="pochta@mail.ru">
                        </div>
                        <p class="red-text form__field_invalid_caption"></p>
                    </div>
          
        </div>`
}
export {OrderFormComponent, AuthOrderFormComponent}