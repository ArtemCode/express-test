import {applyInvalidStyles, formValidator} from "/assets/js/common/FormValidator.js";
import getNumberPhone from "/assets/js/utils/getNumberPhone.js";
import limitInputLength from "/assets/js/common/SMSlength.js";
import {authQuery} from "/assets/js/queries/AuthQueries.js";
import confirmPhoneQuery from "/assets/js/queries/ConfirmPhoneOrder.js";
import {addToStorage, getFromStorage, removeFromStorage} from "/assets/js/utils/localStorageExplorer.js";

import maskPhone from "/assets/js/common/PhoneMask.js";
import {getCartData} from "/assets/js/common/Cart.js";


const LoginForm = {
    init() {
        maskPhone("#register-phone")
        let fetchedPhone = null

        const loginBtn = document.getElementById("login_btn")
        const loginForm = document.getElementById("login-form")

        const smsCodeInput = document.getElementById("login-sms")
        const smsForm = document.getElementById("login-sms-form")
        const smsBtn = document.getElementById("login-sms_btn")

        smsCodeInput.oninput = limitInputLength

        const getLoginData = () => {
            const inputFields = document.querySelectorAll('#login-form .form__field_input');
            const formData = {};

            inputFields.forEach(input => {
                const fieldName = input.dataset.field;
                const isNotSMS = fieldName !== "sms"

                if (isNotSMS) {

                    const isPhone = fieldName === "phone"
                    const fieldValue = isPhone ? getNumberPhone(input.value) : input.value;

                    formData[fieldName] = fieldValue;
                }

            });
            return formData;
        }

        const goToCodeStep = () => {
            loginForm.style.display = "none"
            smsForm.style.display = "flex"
        }
        const handleLogin = async () => {
            const loginData = getLoginData()

            const validationRules = {
                phone: {required: true, phone: true, label: 'Телефон'}

            };
            const validationErrors = formValidator(loginData, validationRules, ["#login-form .form__field_input"]);

            const isValid = !validationErrors.length

            if (isValid) {
                const loginResponse = await authQuery({...loginData})

                if (loginResponse.status) {
                    fetchedPhone = loginData.phone

                    addToStorage("login_confirm_phone", loginData.phone)
                    goToCodeStep()

                    return;
                }
                applyInvalidStyles(["#login-form .form__field_input"], "phone", loginResponse.message)

            }
        }

        const handleConfirmPhone = async () => {
            if (fetchedPhone) {
                const smsVal = smsCodeInput.value

                if(smsVal.length === 6) {
                    const smsCodeVal = Number(smsVal)

                    const {status} = await confirmPhoneQuery({
                        phone: fetchedPhone,
                        code: smsCodeVal
                    }, "auth")

                    if (status) {
                        removeFromStorage("login_confirm_phone")
                        const cartData = await getCartData()
                        window.location.href = "profile.html"
                        return;
                    }
                    applyInvalidStyles(["#login-sms-form .form__field_input"], "sms", "Неверный код.")

                    return;
                }
                applyInvalidStyles(["#login-sms-form .form__field_input"], "sms", "Требуется 6 символов.")

            }
        }

        smsBtn.onclick = handleConfirmPhone

        if (fetchedPhone) {
            goToCodeStep()
            return
        }

        loginBtn.onclick = handleLogin

    }
}
export default LoginForm