import {clearCart, getCartData, getProductsIDS} from "/assets/js/common/Cart.js";
import formatDate from "/assets/js/utils/formateDate.js";
import formValidator, {applyInvalidStyles} from "/assets/js/common/FormValidator.js";
import getNumberPhone from "/assets/js/utils/getNumberPhone.js";
import limitInputLength from "/assets/js/common/SMSlength.js";
import {sendOrderAnonQuery, sendOrderQuery} from "/assets/js/queries/OrderQuery.js";
import confirmPhoneQuery from "/assets/js/queries/ConfirmPhoneOrder.js";
import {addToStorage, getFromStorage, removeFromStorage} from "/assets/js/utils/localStorageExplorer.js";
import {AuthOrderFormComponent, OrderFormComponent} from "/assets/js/components/OrderForm.js"
import {getAuthStatus, getEmail} from "/assets/js/queries/AuthQueries.js";
import DatePicker from "/assets/js/common/DatePicker.js";
import SelectableFields from "/assets/js/common/Selectable.js";
import maskPhone from "/assets/js/common/PhoneMask.js";

const OrderForm = {
    async init() {
        let paymentNeeded = true

        const orderForm = document.getElementById("order-form")
        const {user_id} = await getAuthStatus()
        const {email} = await getEmail()
        orderForm.innerHTML = user_id ? AuthOrderFormComponent(email) : OrderFormComponent()

        maskPhone("#order-phone")
        DatePicker.init();
        DatePicker.orderBirthdayInit()
        SelectableFields.init();


        const btnOrder = document.getElementById("order_btn")
        const btnOrderCenter = document.getElementById("order_btn_center")
        const smsCodeInput = document.getElementById("order-sms")

        const smsForm = document.getElementById("order-sms-form")
        const smsBtn = document.getElementById("order-sms_btn")


        let fetchedPhone = getFromStorage("order_confirm_phone") || null

        smsCodeInput.oninput = limitInputLength

        const getOrderData = async () => {
            const inputFields = document.querySelectorAll('.order .form__field_input');

            const selectedGender = document.querySelector('.form__selectable-selected');
            const sex = selectedGender ? selectedGender.textContent : '';
            const formData = {};

            inputFields.forEach(input => {

                const fieldName = input.dataset.field; // Получаем имя поля из атрибута data-field
                const isDate = fieldName === "dob" || fieldName === "birthday"
                const isPhone = fieldName === "phone"
                const isTime = fieldName === "time"

                const fieldValue = isPhone ?
                    getNumberPhone(input.value) : isDate ?
                        formatDate(input.value) : input.value

                if (!isTime) {
                    formData[fieldName] = fieldValue;
                } else {
                    if(formData["dob"].length) {
                        formData["dob"] += ` ${input.value}`
                    }

                }
                // Добавляем поле и значение в объект formData
            });
            if (!formData['sex']) {
                formData['sex'] = sex != "Мужчина" ? 0 : 1;
            }



            const cartData = await getCartData()
            const cartProductsIDs = getProductsIDS(cartData)


            return {
                ...formData,
                products: cartProductsIDs
            }
        }

        const goToCodeStep = () => {
            orderForm.style.display = "none"
            smsForm.style.display = "flex"
        }

        const handleOrder = async (paymentNeeded = true) => {

            const orderData = await getOrderData()

            const validationRules = !user_id ? {
                first_name: {required: true, label: 'Имя'},
                last_name: {required: true, label: 'Фамилия'},
                surname: {required: true, label: 'Отчество'},
                dob: {required: true, date: true, label: "Дата"},
                birthday: {required: true, date: true, label: "Дата рождения"},
                phone: {required: true, phone: true, label: 'Телефон'},
                email: {required: true, email: true, label: 'Email'},
            } : {
                dob: {required: true, date: true, label: "Дата"},
                email: {required: true, email: true, label: 'Email'},
            };


            const validationErrors = formValidator(orderData, validationRules, [".order__form .form__field_input"]);

            const isValid = !validationErrors.length


            if (isValid) {
                const orderResponse = !user_id ?
                    await sendOrderAnonQuery({...orderData}) :
                    await sendOrderQuery({...orderData})


                if (orderResponse.status) {
                    fetchedPhone = orderData.phone

                    if (!user_id) {
                        addToStorage("order_confirm_phone", orderData.phone)
                        goToCodeStep()
                        return
                    } else {
                        const paymentURL = "/lk/payment/create/" + orderResponse.message
                        window.location.href = paymentNeeded ? paymentURL : "/lk/payment-success"
                    }

                } else {
                    applyInvalidStyles(["#order-form .form__field_input"], "phone", orderResponse.code)
                }

                return;

            }
        }

        const handleConfirmPhone = async () => {
            if (fetchedPhone) {
                const smsVal = smsCodeInput.value
                if (smsVal.length === 6) {
                    const smsCodeVal = Number(smsVal)
                    const confirmRes = await confirmPhoneQuery({
                        phone: fetchedPhone,
                        code: smsCodeVal

                    }, "create_order")

                    if (confirmRes.status) {
                        clearCart()
                        removeFromStorage("order_confirm_phone")
                        const paymentURL = "/lk/payment/create/" + confirmRes.message
                        window.location.href = paymentNeeded ? paymentURL : "/lk/payment-success"
                        return;
                    }
                    applyInvalidStyles(["#order-sms-form .form__field_input"], "sms", "Неверный код.")
                    return;


                }
                applyInvalidStyles(["#order-sms-form .form__field_input"], "sms", "Требуется 6 символов.")


            }
        }
        smsBtn.onclick = handleConfirmPhone

        if (fetchedPhone) {
            goToCodeStep()
            return
        }
        btnOrder.onclick = () =>{
            paymentNeeded = true
            handleOrder(paymentNeeded)
        }
        btnOrderCenter.onclick = () => {
            paymentNeeded = false
            handleOrder(paymentNeeded)
        }

    }
}
export default OrderForm