import {applyInvalidStyles, formValidator} from "/assets/js/common/FormValidator.js";
import getNumberPhone from "/assets/js/utils/getNumberPhone.js";
import limitInputLength from "/assets/js/common/SMSlength.js";
import {registerQuery} from "/assets/js/queries/AuthQueries.js";
import confirmPhoneQuery from "/assets/js/queries/ConfirmPhoneOrder.js";
import {addToStorage, getFromStorage, removeFromStorage} from "/assets/js/utils/localStorageExplorer.js";
import maskPhone from "/assets/js/common/PhoneMask.js";
import formatDate from "/assets/js/utils/formateDate.js";
function smoothscroll(){
    var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
    if (currentScroll > 0) {
        window.requestAnimationFrame(smoothscroll);
        window.scrollTo (0,currentScroll - (currentScroll/5));
    }
}

const RegisterForm = {
    init() {
        maskPhone("#register-phone")

        let fetchedPhone = getFromStorage("reg_confirm_phone") || null
        const registerBtn = document.getElementById("register_btn")
        const registerForm = document.getElementById("register-form")

        const smsCodeInput = document.getElementById("register-sms")
        const smsForm = document.getElementById("register-sms-form")
        const smsBtn = document.getElementById("register-sms_btn")

        smsCodeInput.oninput = limitInputLength

        const getRegisterData = () => {
            const inputFields = document.querySelectorAll('.auth .form__field_input');
            const selectedGender = document.querySelector('.form__selectable-selected');
            const sex = selectedGender ? selectedGender.textContent : '';
            const formData = {};

            inputFields.forEach(input => {
                const fieldName = input.dataset.field;
                const isNotSMS = fieldName !== "sms"

                if (isNotSMS) {
                    const isDate = fieldName === "birthday"
                    const isPhone = fieldName === "phone"
                    const fieldValue = isPhone ?
                        getNumberPhone(input.value) : isDate ?
                            formatDate(input.value) : input.value

                    formData[fieldName] = fieldValue;
                }

            });

            if (!formData['sex']) {
                formData['sex'] = sex != "Мужчина" ? 0 : 1;
            }

            return formData;
        }

        const goToCodeStep = () => {
            registerForm.style.display = "none"
            smsForm.style.display = "flex"
            smoothscroll()
        }

        const handleRegister = async () => {
            const registerData = getRegisterData()

            const validationRules = {
                first_name: {required: true, label: 'Имя'},
                last_name: {required: true, label: 'Фамилия'},
                surname: {required: true, label: 'Отчество'},
                phone: {required: true, phone: true, label: 'Телефон'},
                birthday: {required: true, date: true, label: "Дата рождения"},
                email: {required: true, email: true, label: 'Email'},

            };
            const validationErrors = formValidator(registerData, validationRules, ["#register-form .form__field_input"]);

            const isValid = !validationErrors.length

            if (isValid) {
                const registerResponse = await registerQuery({...registerData})

                if (registerResponse.status) {
                    fetchedPhone = registerData.phone

                    addToStorage("reg_confirm_phone", registerData.phone)
                    goToCodeStep()
                    const element = document.documentElement; // Или другой элемент, который вы хотите прокрутить до него
                    element.scrollIntoView({ behavior: 'smooth', block: 'start' });
                    return;

                }

                applyInvalidStyles(["#register-form .form__field_input"], "phone", registerResponse.code)


            }
        }

        const handleConfirmPhone = async () => {
            if (fetchedPhone) {
                const smsVal = smsCodeInput.value

                if (smsVal.length === 6) {
                    const smsCodeVal = Number(smsVal)

                    const confirmResponse = await confirmPhoneQuery({
                        phone: fetchedPhone,
                        code: smsCodeVal
                    }, "reg")

                    if (confirmResponse.status) {
                        removeFromStorage("reg_confirm_phone")
                        window.location.href = "profile.html"
                        return;
                    }
                    applyInvalidStyles(["#register-sms-form .form__field_input"], "sms", "Неверный код.")

                    return;
                }
                applyInvalidStyles(["#register-sms-form .form__field_input"], "sms", "Требуется 6 символов.")

            }
        }
        smsBtn.onclick = handleConfirmPhone


        if (fetchedPhone) {
            goToCodeStep()
            document.body.scrollTop = document.documentElement.scrollTop = 0;
            return
        }

        registerBtn.onclick = handleRegister

    }
}
export default RegisterForm