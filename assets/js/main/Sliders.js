import {addToCart, getCartData, isProductInCart, renderAdded} from "/assets/js/common/Cart.js";

const MainSliders = {

    init: function () {
        const addProductInSliderItem = async (e, item, productId) => {
            e.preventDefault();
            const cartData = await getCartData(true)
            const alreadyInCart = isProductInCart(productId, cartData)

            if (!alreadyInCart) {
                const added = await addToCart(productId, cartData);

                if (added) {
                    renderAdded(item)
                }
                return;
            }

            renderAdded(item)

        };

        getCartData(true)
            .then(cartData => {
                const popularAnalyzesSliders = $(".pa__slider")
                const allBtnsInSliders = document.querySelectorAll(".slider_item_btn");

                popularAnalyzesSliders.each((i, paSlider) => {
                    const prevBtn = paSlider.parentElement.children[0].children[0]
                    const nextBtn = paSlider.parentElement.children[1].children[0]
                    const slider = $(paSlider)

                    slider.slick({
                        infinite: false,
                        slidesToShow: 4,
                        slidesToScroll: 1,
                        prevArrow: $(prevBtn),
                        nextArrow: $(nextBtn),
                        responsive: [
                            {
                                breakpoint: 1361,
                                settings: {
                                    slidesToShow: 3,
                                }
                            },
                            {
                                breakpoint: 1015,
                                settings: {
                                    slidesToShow: 2,

                                }
                            },
                            {
                                breakpoint: 801,
                                settings: {
                                    slidesToShow: 1,
                                    swipe: true

                                }
                            }
                        ]
                    })
                    slider.on('afterChange', function (event, slick, currentSlide) {
                        const slidesToShow = slider.slick('slickGetOption', 'slidesToShow');
                        const slideCount = slider.slick("getSlick").slideCount;

                        if (currentSlide > 0) {
                            if (prevBtn.classList.contains("v-hidden")) {
                                prevBtn.classList.remove("v-hidden")
                            }

                            if (currentSlide === slideCount - slidesToShow) {
                                nextBtn.classList.add("v-hidden")
                            } else {
                                nextBtn.classList.remove("v-hidden")
                            }
                        } else {
                            prevBtn.classList.add("v-hidden")
                        }


                    });
                })

                allBtnsInSliders.forEach(item => {
                    const productId = Number(item.getAttribute("data-product-id"));

                    if (isProductInCart(productId, cartData)) {
                        renderAdded(item)
                    }

                    item.onclick = (e) => addProductInSliderItem(e, item, productId)

                });
            })
    },
    promoSlidersInit: function () {


        const slider = $(".promo__slider")
        const prevBtn = $(".promo__pag_prev")
        const nextBtn = $(".promo__pag_next")
        const sliderDotsParent = document.getElementById("promo-slider-dots")
        const sliderPagination = document.getElementById("promo-pag")

        const updateSliderDots = (count, activeSlide, neededRender = false) => {
            if(neededRender) {
                for (let i = 0; i < count; i++) {
                    sliderDotsParent.innerHTML += `<div class="promo__slide_dot"></div>`
                }
            }
            const sliderDots = document.querySelectorAll(".promo__slide_dot")

            sliderDots.forEach((item, index) => {
                item.classList.remove("promo__slide_dot-active")
                if(activeSlide === index) {
                    item.classList.add("promo__slide_dot-active")
                }
            })
        }
        const sliderList = document.querySelectorAll('.promo__slide_wrapper')
        let maxHeightSlide = sliderList[0].offsetHeight
        const handleSlider = () => {
            if(window.innerWidth > 800) {
                slider.slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    fade: true,
                    dots: false,
                    prevArrow: $(prevBtn),
                    nextArrow: $(nextBtn)
                })
                const sliderObject = slider.slick("getSlick");
                const sliderCount = sliderObject.slideCount;

                if(sliderCount == 1) {
                    sliderPagination.style.display = "none"
                }

                updateSliderDots(sliderCount, 0, true)

                slider.on('afterChange', function(event, slick, currentSlideIndex){
                    updateSliderDots(sliderCount, currentSlideIndex)
                });

                sliderList.forEach(item => {
                    if(item.offsetHeight > maxHeightSlide ) {
                        maxHeightSlide = item.offsetHeight
                    }

                })

                sliderList.forEach(item => {
                    item.style.height = `${maxHeightSlide}px`

                })
            }
        }
        handleSlider()

        window.addEventListener("resize", e => {
           handleSlider()
        })

    },
    promoMobileSliderInit: function () {
        const mobileSlider = $(".promo_mb_slider")
        const prevBtn = $(".promo__mb_pag_next")
        const nextBtn = $(".promo__mb_pag_prev")
        const promoMbPag = document.getElementById("promo-mb-pag")
        const promoMbCount = document.getElementById("promo-mb-count")

        mobileSlider.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: true,
            dots: false,
            prevArrow: $(prevBtn),
            nextArrow: $(nextBtn)
        })
        const sliderObject = mobileSlider.slick("getSlick");
        const sliderCount = sliderObject.slideCount;

        if(sliderCount == 1) {
            promoMbPag.style.display = "none"
        }
        const updateSlideCount = (currentSlide, sliderCount) => {
            promoMbCount.textContent = `${currentSlide + 1} из ${sliderCount}`
        }
        updateSlideCount(0, sliderCount)
        mobileSlider.on('afterChange', function(event, slick, currentSlideIndex){
            updateSlideCount(currentSlideIndex, sliderCount)
        });
        const sliderList = document.querySelectorAll('.promo__mb_slide .promo__mb_content-wrapper')
        let maxHeightSlide = sliderList[0].offsetHeight

        sliderList.forEach(item => {
            if(item.offsetHeight > maxHeightSlide ) {
                maxHeightSlide = item.offsetHeight
            }

        })



        sliderList.forEach(item => {
            item.style.height = `${maxHeightSlide}px`

        })

    },
    slidesTextCorrect: function () {
        const slideBlocks = document.querySelectorAll(".pa__slider_slide")

        slideBlocks.forEach(item => {
            const title = item.querySelector("h3")
            const description = item.querySelector(".slider_item_days")

            const titleHeight = title.offsetHeight

            if(titleHeight > 50) {
                description.style.maxHeight = "1.9rem"
                description.style.webkitLineClamp = "1"
            } else if(titleHeight < 50 && titleHeight > 30) {
                description.style.maxHeight = "3.9rem"
                description.style.webkitLineClamp = "2"
            } else if(titleHeight < 25) {
                description.style.maxHeight = "5.8rem"
                description.style.webkitLineClamp = "3"
            }

        })
    }

}
export default MainSliders


