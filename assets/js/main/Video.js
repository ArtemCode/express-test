const VideoBackground = {
    init: () => {
        const videoToggle = () => {
            const isMobile = window.innerWidth <= 800
            if(isMobile) {
                mainVideo.pause()
                //mainVideo.innerHTML = ""

            } else {
                mainVideo.play()
                //mainVideo.innerHTML = `<source src="assets/img/videos/main_video.mp4" type="video/mp4">`
            }
        }
        const mainVideo = document.getElementById("get-results-video")

        videoToggle()
    }

}

export default VideoBackground