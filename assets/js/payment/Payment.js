import {paymentCreate} from "/assets/js/queries/PaymentQueries.js";

const PUBLIC_ID = "pk_75b3be8c0a802f57b1404315d17c9"
//old http://dev.advafert.ru/lk/pyment/3ds
const setPayState = (stateNumber) => {
    const paymentStatesBlocks = document.querySelectorAll(".pay-state")
    paymentStatesBlocks.forEach(item => {
        item.classList.remove('pay-current-state')
    })
    paymentStatesBlocks[stateNumber].classList.add("pay-current-state")

}
const setPaymentError = errMsg => {
    const errNode = document.getElementById("payment-error")
    errNode.textContent = errMsg
}
const createSend3dsForm = (acsURL, paReq, termURL, md) => {
    const formContainer = document.getElementById("form-3ds")
    const form = document.createElement('form');
    form.action = acsURL;
    form.method = 'POST';

    const inputPaReq = document.createElement('input');
    inputPaReq.type = 'hidden';
    inputPaReq.name = 'PaReq';
    inputPaReq.value = paReq;
    form.appendChild(inputPaReq);

    const inputMD = document.createElement('input');
    inputMD.type = 'hidden';
    inputMD.name = 'MD';
    inputMD.value = md;
    form.appendChild(inputMD);

    const inputTermURL = document.createElement('input');
    inputTermURL.type = 'hidden';
    inputTermURL.name = 'TermUrl';
    inputTermURL.value = termURL;
    form.appendChild(inputTermURL);

    formContainer.appendChild(form);
    // Обработчик успешного результата отправки формы
    form.submit()
}
function extractIdFromCurrentUrl() {
    const regex = /\/(\d+)$/;
    const match = window.location.href.match(regex);
    if (match && match.length >= 2) {
        return parseInt(match[1], 10);
    } else {
        return null; // Вернуть null, если число айди не найдено или невалидно
    }
}

const createdCryptoProgramHandle = async (cpp) => {
    const orderId = extractIdFromCurrentUrl();
    const createdPayment = await paymentCreate(orderId, cpp)

    if(createdPayment["3ds"]) {
        const redirectAcsURL = createdPayment?.AcsUrl
        const mdParam = createdPayment?.MD
        const paReqParam = createdPayment?.PaReq
        const termURL = createdPayment?.TermUrl

        createSend3dsForm(redirectAcsURL, paReqParam, termURL, mdParam)
        return;
    }
    if(createdPayment["status"]) {
        window.location.href = "/lk/payment-success"
    }
}
function createCheckout(cardData) {
    setPaymentError("")
    const checkout = new cp.Checkout({
        publicId: PUBLIC_ID,
    });

    checkout.createPaymentCryptogram(cardData)
        .then(async (cryptogram) => {
            await createdCryptoProgramHandle(cryptogram)
        })
        .catch(() => {
            setPaymentError("Карточные данные некорректны.")
            setPayState(0)
        });

}

const Payment = {
    init() {
        const cardData = {
            number: false,
            expire: false,
            cvv: false
        }

        const checkValidToggleDisabled = () => {
            const isCardValid = cardData.cvv && cardData.number && cardData.expire

            if (isCardValid) {
                payBtn.disabled = false
                return isCardValid;
            }
            payBtn.disabled = true

            return isCardValid
        }

        const payForm = document.getElementById("pay-form")
        const payBtn = document.getElementById("payment-btn")

        const cardNumber = $("#card-number")
        const cardExpire = $("#card-expire")
        const cardCVV = $("#card-cvv")

        cardNumber.inputmask("9999 9999 9999 9999")
        cardExpire.inputmask("99/99")
        cardCVV.inputmask("999")

        cardNumber.on('input', e => {
            const inputValue = e.target.value

            if (inputValue.length === 19 && !inputValue.includes("_")) {
                cardExpire.focus()
                cardData.number = inputValue
            } else {
                cardData.number = false
            }

            checkValidToggleDisabled()

        })
        cardExpire.on('input', e => {
            const inputValue = e.target.value
            const expireSplitted = inputValue.split("/")

            const monthInt = Number(expireSplitted[0])

            if (inputValue.length === 5 && !inputValue.includes("_")) {
                cardCVV.focus()
                cardData.expire = inputValue

                if (monthInt > 12) {
                    const correctedExpire = `12/${expireSplitted[1]}`
                    cardExpire.val(correctedExpire)
                    cardData.expire = correctedExpire
                }
            } else {
                cardData.expire = false
            }
            checkValidToggleDisabled()

        })
        cardCVV.on('input', e => {
            const inputValue = e.target.value
            if (inputValue.length === 3 && !inputValue.includes("_")) {
                cardCVV.focus()
                cardData.cvv = inputValue
            } else {
                cardData.cvv = false
            }
            checkValidToggleDisabled()

        })

        const getCardNormalized = (data = cardData) => {
            const expireSplitted = data.expire.split("/")

            return {
                cvv: data.cvv,
                cardNumber: data.number,
                expDateMonth: expireSplitted[0],
                expDateYear: expireSplitted[1],
            }

        }

        payForm.onsubmit = e => {
            e.preventDefault()

            if (checkValidToggleDisabled()) {
                const cardNormalized = getCardNormalized()
                setPayState(1)
                createCheckout(cardNormalized)
            }

        }
    }

}
export default Payment