import {getProductById} from "/assets/js/queries/ProductsQueries.js";
import {addToCart, getCartData, isProductInCart, renderAdded} from "/assets/js/common/Cart.js";


const Product = {
    init() {
        const currentURL = window.location.href;
        const urlParts = currentURL.split('/');
        //const tempID = Number(urlParts[urlParts.length - 1]);
        const tempID = 28

        getProductById(tempID).then(async res => {
            const productCodeBlock = document.getElementById("product-code")
            const productDescriptionBlock = document.getElementById("product-content")
            const productBtnAdd = document.getElementById("product-btn-add")
            const productPriceBlocks = document.querySelectorAll(".product-price")
            const breadCrumbsName = document.getElementById("breadcrumbs-name")

            const {status} = res
            const cartData = await getCartData(true)


            if (status) {
                const alreadyInCart = isProductInCart(tempID, cartData)
                const {info, code, cost, name} = res.analiz
                breadCrumbsName.textContent = name
                productCodeBlock.textContent = code

                if (info) {
                    let conv = new showdown.Converter()
                    const convertedDescription = conv.makeHtml(info)
                    productDescriptionBlock.innerHTML = convertedDescription
                } else {
                    productDescriptionBlock.innerHTML = `
                        <h2>${name}</h2>
                        <p>К сожалению, для данного продукта описание отсутствует.</p>
                    `
                }


                productPriceBlocks.forEach(item => {
                    item.innerHTML = cost
                })

                if (alreadyInCart) {
                    renderAdded(productBtnAdd)
                }

                productBtnAdd.onclick = async (e) => {
                    const btnBlock = e.currentTarget

                    const cartData = await getCartData(true)
                    const alreadyInCart = isProductInCart(tempID, cartData)

                    if (!alreadyInCart) {
                        const added = await addToCart(tempID, cartData);

                        if (added) {
                            renderAdded(btnBlock)
                        }
                        return;
                    }

                    renderAdded(btnBlock)
                }
            }


        })
    }
}

export default Product