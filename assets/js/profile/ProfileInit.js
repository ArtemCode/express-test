import maskPhone from "/assets/js/common/PhoneMask.js";
import initTabs from "/assets/js/common/Tabs.js";
import {customDatepicker} from "/assets/js/common/DatePicker.js";

const setQuizStep = (stepID) => {
    const paymentStatesBlocks = document.querySelectorAll(".modal-do__quiz-item")
    paymentStatesBlocks.forEach(item => {
        item.classList.remove('modal-do__quiz-current')
    })
    paymentStatesBlocks[stepID].classList.add("modal-do__quiz-current")
}

const setCurrentOrderDoctorData = (id) => {

}

const doctorsOrderInit = () => {
    const doctors = document.querySelectorAll(".profile__doctor")
    doctors.forEach(item => {
        const orderBtn = item.querySelector('.profile__btn')
        const docID = item.dataset.doctorId
        orderBtn.addEventListener('click', () => {
            setCurrentOrderDoctorData(docID)
        })
    })
}
const modalDoctorOrderInit = () => {
    customDatepicker("#modal-do-datepicker", ".modal-do-date")
}

const Profile = {
    init() {
        maskPhone("#profile-phone")
        initTabs(".profile__bar_tab", ".profile__tab", "profile__tab-selected", "profile-current-tab")

        // **** ИЗМЕНЕНИЕ ПОЛА
        const inputElement = document.getElementById('profile-input-gender');
        const selectableLabels = document.querySelectorAll('.form__selectable_wrapper label');

        // Устанавливаем класс выбранному элементу на основе значения input
        selectableLabels.forEach(label => {
            if (label.textContent.trim() === inputElement.value.trim()) {
                label.classList.add('form__selectable-selected');
            }
        });
        selectableLabels.forEach(label => {
            label.addEventListener('click', () => {
                const value = label.textContent.trim();
                inputElement.setAttribute("value", value)

                // Удаление класса у всех блоков <label>
                selectableLabels.forEach(label => {
                    label.classList.remove('form__selectable-selected');
                });

                label.classList.add('form__selectable-selected');

            });
        });

        // **** ОТКЛЮЧЕНИЕ ВСЕХ ПОЛЕЙ ВВОДА

        const disabledFields = document.querySelectorAll('.form__field-disabled input');

        disabledFields.forEach(input => {
            input.setAttribute('readonly', true);
        });
        doctorsOrderInit()
        modalDoctorOrderInit()
    }
}
export default Profile