import sendRequest from "/assets/js/queries/Query.js";

const defaultRegisterData = {
    phone: 0,
    first_name: "",
    last_name: "",
    surname: ""
}

const registerQuery = async (data = defaultRegisterData) => {
    return await sendRequest(
        "/lk/reg",
        "POST",
        null,
        {...data}
    )
}

const authQuery = async (data = defaultRegisterData) => {
    return await sendRequest(
        "/lk/auth",
        "POST",
        null,
        {...data}
    )
}
const getEmail = async () => {
    return await sendRequest(
        "/check/email",
        "GET",
        null
    )
}

let cachedAuthStatus = null
const getAuthStatus = async () => {
    try {
        if(!cachedAuthStatus) {
            const {user_id} = await sendRequest(
                "/check/auth",
                "POST",
                null
            )
            const authUserID = {
                user_id
            }
            cachedAuthStatus = authUserID

            return 0
        }
        return cachedAuthStatus
    }
    catch (e) {
        console.log(e)
    }


}



export {authQuery, registerQuery, getAuthStatus, getEmail}
