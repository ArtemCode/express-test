import sendRequest from "/assets/js/queries/Query.js";

const cartURL = '/lk/basket'

const cartAdd = async (productIds = []) => {
    return await sendRequest(
        cartURL + "/add",
        "POST",
        null,
        {
            list_product_id: productIds
        }
    )
}

const cartDelete = async (productId) => {


    return await sendRequest(
        cartURL + "/del",
        "POST",
        null,
        {
            basket_id: Number(productId)
        }
    )
}

let cachedCart = null
const getCart = async (neededCache = false) => {

    if(!neededCache) {

        const cartData = await sendRequest(
            cartURL,
            "POST",
            null
        )
        cachedCart = cartData
        return cartData

    }
    if(!cachedCart) {

        return cachedCart
    }

    return cachedCart

}


export {
    cartAdd,
    getCart,
    cartDelete
}

