import sendRequest from "/assets/js/queries/Query.js";

const confirmPhoneQuery = async (data = {phone: 0, code: 0}, typeForm = "") => {
    const typeForms = [
        "reg",
        "auth",
        "create_order"
    ]

    if(typeForms.includes(typeForm)) {
        return await sendRequest(
            `/lk/phone/confirm/${typeForm}`,
            "POST",
            null,
            {...data}
        )
    }

}
export default confirmPhoneQuery
