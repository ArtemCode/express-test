import sendRequest from "/assets/js/queries/Query.js";

const getDocDates = async (docID) => {
    return await sendRequest(
        `/lk/doctors/get-dates`,
        "POST",
        null,
        {
            doctor_id: docID
        }
    )
}

const getDocTimes = async (docID) => {
    return await sendRequest(
        `/lk/payment/create`,
        "POST",
        null,
        {
            id: docID
        }
    )
}
export {
    getDocDates,
    getDocTimes
}