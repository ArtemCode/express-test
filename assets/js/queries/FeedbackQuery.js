import sendRequest from "/assets/js/queries/Query.js";

const defaultData = {
    form_id: 1,
    fields: []
}

const sendFeedbackQuery = async(data = defaultData, headers)  => {
    return await sendRequest(
        "/post-form",
        "POST",
        null,
        {...data},
        {...headers}
    )
}


const getFeedbackQuery = async(pageID) => {

    const feedbackFields = await sendRequest(
        "/get-form",
        "POST",
        null,
        {
            "id_page": pageID
        }
    )


    return feedbackFields

}

export {sendFeedbackQuery, getFeedbackQuery}

