import sendRequest from "/assets/js/queries/Query.js";

const defaultData = {
    products: [1, 2, 3],
    first_name: "first_name",
    last_name: "last_name",
    surname: "surname",
    dob: "2023-06-30 14:50",
    sex: "пол",
    phone: 79997911240,
    email: "admin@advafert.ru"
}


const sendOrderAnonQuery = async(data = defaultData, headers)  => {
    return await sendRequest(
        "/lk/order/create/anone",
        "POST",
        null,
        {...data},
        {...headers}
    )
}
const sendOrderQuery = async(data = defaultData, headers)  => {
    return await sendRequest(
        "/lk/api/order/create",
        "POST",
        null,
        {...data},
        {...headers}
    )
}


export {sendOrderAnonQuery, sendOrderQuery}