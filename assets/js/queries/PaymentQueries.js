import sendRequest from "/assets/js/queries/Query.js";

const paymentCreate = async (orderID, cryptoKey) => {
    return await sendRequest(
        `/lk/payment/create/${orderID}`,
        "POST",
        null,
        {
            CCP: cryptoKey
        }
    )
}

export {
    paymentCreate
}