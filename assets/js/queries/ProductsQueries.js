import sendRequest from "/assets/js/queries/Query.js";

const getProductsByName = async (search, pag = 1) => {
    return await sendRequest(
        "/",
        "POST",
        null,
        {
            "analiz": search,
            "pag": pag
        }
    )
}
const getProductById = async (id) => {
    return await sendRequest(
        `/get-analysis/${id}`,
        "POST"
    )
}


const getProductDescription = async (id) => {
    return await sendRequest(
        `/desc-analysis/${id}`,
        "POST"
    )
}



const getProductCategories = async () => {
    return await sendRequest(
        `/analysis/get-category`,
        "GET",
    )
}

const getProductsByCategory = async (data = {
    analiz: "",
    category: [],
    pag: 1
}) => {
    return await sendRequest(
        `/analysis`,
        "POST",
        null,
        {...data}
    )
}


export {
    getProductById,
    getProductsByName,
    getProductDescription,
    getProductsByCategory,
    getProductCategories
}