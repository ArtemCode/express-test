function getCookie(name) {
    // Получаем все куки для данной страницы
    const cookies = document.cookie.split(';');

    // Ищем куки с заданным именем и возвращаем его значение
    for (let i = 0; i < cookies.length; i++) {
        const cookie = cookies[i].trim();

        if (cookie.startsWith(name + '=')) {
            return cookie.substring(name.length + 1);
        }
    }

    // Если куки с заданным именем не найдены, возвращаем пустую строку
    return '';
}

async function sendRequest(
    url,
    method = 'GET',
    params = null,
    data = null,
    headers = {}) {
    // Формирование полного URL с параметрами запроса
    const baseURL = "http://vps.advafert.ru:8080"
    let endpoint = baseURL + url
    if (params) {
        const queryParams = new URLSearchParams(params);
        endpoint += '?' + queryParams.toString();
    }

    const options = {
        method,
        headers: {
            ...headers,
            "Content-Type": "application/json",
            // 'X-CSRFToken': getCookie('csrftoken')
        },
        body: data ? JSON.stringify(data) : null
    }
    try {
        const response = await fetch(endpoint, options);
        const responseData = await response.json();
        return responseData;
    } catch (e) {
        console.log(e)
    }

}

export default sendRequest