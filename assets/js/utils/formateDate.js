const formatDate = (inputDate) => {

    const parts = inputDate.split('.');

    const day = parts[0];
    const month = parts[1];
    const year = parts[2];
    if (!year) {
        return ""
    }
    // Формирование строки в формате "yyyy-mm-dd hh:mm"
    const formattedDate = `${year}-${month}-${day}`;

    return formattedDate;


}

export default formatDate
