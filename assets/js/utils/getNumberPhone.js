function convertPhoneNumber(phoneNumber) {
    const digits = phoneNumber.replace(/\D/g, '');
    return parseInt(digits);
}
export default convertPhoneNumber