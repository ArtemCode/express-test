const addToStorage = (storageName, val) => {
    localStorage.setItem(storageName, JSON.stringify(val))
}

const getFromStorage = (storageName) => {
    const stored = localStorage.getItem(storageName) || ''
    if (!stored) {
        return ''
    }
    return JSON.parse(stored)
}
const removeFromStorage = (storageName) => {
    localStorage.removeItem(storageName);
};

export {addToStorage, getFromStorage, removeFromStorage}