import getNumberPhone from "/assets/js/utils/getNumberPhone.js";

function removePostfixFromFieldTypes(obj) {
    const { type, data } = obj;
    const newType = type.replace(/_\d+$/, ''); // Удалить постфикс с числом
    return { type: newType, data };
}






export default removePostfixFromFieldTypes
